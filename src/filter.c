/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <regex.h>
#include <stdbool.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"
#include "env.h"
#include "filter.h"

#define token_comparator 1
#define token_leftop     2
#define token_rightop    4

typedef enum _operator {
    operator_eq,
    operator_ne,
    operator_gte,
    operator_lte,
    operator_gt,
    operator_lt,
    operator_regex,
    operator_flag,
    operator_and,
    operator_or,
    operator_not,
    operator_group,
} operator_t;

typedef enum _token {
    token_eq,
    token_ne,
    token_gte,
    token_lte,
    token_gt,
    token_lt,
    token_regex,
    token_and,
    token_or,
    token_not,
    token_openbracket,
    token_closebracket,
    token_whatever,
    token_finish,
} token_t;

struct _filter {
    filter_t * parent;
    operator_t operator;
    filter_t * op1;
    filter_t * op2;
    char *param;
    variant_t value;
    regex_t regvalue;
};

typedef struct _token_descriptor token_descriptor_t;

struct _token_descriptor {
    token_t key;
    char *string;
    unsigned int len;
    int flags;
    operator_t operator;
};

static token_descriptor_t token[] = {
    {token_eq,           "==", 2, token_comparator             , operator_eq    },
    {token_eq,           "=",  1, token_comparator             , operator_eq    },
    {token_ne,           "!=", 2, token_comparator             , operator_ne    },
    {token_gte,          ">=", 2, token_comparator             , operator_gte   },
    {token_lte,          "<=", 2, token_comparator             , operator_lte   },
    {token_gt,           ">",  1, token_comparator             , operator_gt    },
    {token_lt,           "<",  1, token_comparator             , operator_lt    },
    {token_regex,        "~",  1, token_comparator             , operator_regex },
    {token_and,          "&&", 2, token_leftop | token_rightop , operator_and   },
    {token_or,           "||", 2, token_leftop | token_rightop , operator_or    },
    {token_not,          "!",  1, token_rightop                , operator_not   },
    {token_openbracket,  "(",  1, token_rightop                , operator_group },
    {token_closebracket, ")",  1, token_leftop                 , operator_group },
    {token_finish,       "]" , 1, token_leftop                 , operator_eq    },
};
static int tokenlen = 14;
static token_descriptor_t tokendefault =
    {token_whatever,     "",   0, 0                            , operator_eq    };
static token_descriptor_t tokenend =
    {token_finish,       "",   0, token_leftop                 , operator_eq    };

static token_descriptor_t *eatToken(char *s, char **endptr) {
    token_descriptor_t *t = NULL;
    int i;
    while (*s && *s <= ' ') // ignore white space and special characters
        s++;
    do {
        if (*s <= ' ') {
            if (!t)
                t = &tokenend;
            break;
        } else {
            for (i=0; i<tokenlen; i++) {
                if (strlen(s) >= token[i].len && !strncmp(token[i].string, s, token[i].len)) {
                    if (!t)
                        t = &token[i];
                    break;
                }
            }
        }
        if (i == tokenlen) {
            if (!t) {
                t = &tokendefault;
                t->string = s;
                t->len = 0;
            }
            s++;
            t->len++;
        } else if (t && t != &tokendefault) {
            s += t->len;
        }
    } while (i == tokenlen);
    while (*s && *s <= ' ') // ignore white space and special characters
        s++;
    *endptr = s;
    return t;
}

filter_t *filter_parse(char * buf, char **endptr) {
    bool r = false;
    char *param = NULL, *value;
    int paramlen = 0;
    filter_t *root = calloc(1, sizeof(filter_t));
    if (!root) {
        logerr("calloc(1, %zu) failed", sizeof(filter_t));
        goto out;
    }
    if (*buf++ != '[') {
        logerr("Filter syntax error: Filter not [-initiated!");
        goto out;
    }
    root->operator = operator_group;
    filter_t *cur = root;
    filter_t *f;
    token_descriptor_t * t;
    do {
        t = eatToken(buf, &buf);
        if (t->key == token_whatever) {
            param = t->string;
            paramlen = t->len;
        } else if ((t->flags & token_comparator) || ((t->flags & token_leftop) && !cur->op2)) {
            if (cur->op2) {
                logerr("Filter syntax error: Comparator operator not expected");
                goto out;
            }
            f = calloc(1, sizeof(filter_t));
            if (!f)
                goto out;
            f->parent = cur;
            if (param) {
                f->param = param;
                *(param + paramlen) = '\0';
            }
            if (t->flags & token_comparator) {
                f->operator = t->operator;
                variant_initialize(&f->value, variant_type_unknown);
                if (variant_parse(&f->value, buf, &buf) != parse_success) {
                    logerr("Syntax error: Invalid filter value for param %s", f->param);
                    filter_destroy(f);
                    goto out;
                }
                if (f->operator == operator_regex) {
                    value = variant_char(&f->value);
                    variant_cleanup(&f->value);
                    if (regcomp(&f->regvalue, value?value:"", REG_EXTENDED)) {
                        logerr("Invalid regexp: %s", value?value:"");
                        filter_destroy(f);
                        free(value);
                        goto out;
                    }
                    free(value);
                }
            } else {
                if (f->param &&
                        strcmp(f->param, "read-only") &&
                        strcmp(f->param, "template") &&
                        strcmp(f->param, "instance") &&
                        strcmp(f->param, "accept-parameters") &&
                        strcmp(f->param, "persistent")) {
                    logerr("Syntax error: %s is not a valid flag name", f->param);
                    filter_destroy(f);
                    goto out;
                }
                f->operator = operator_flag;
            }
            cur->op2 = f;
            param = NULL;
            while (cur->operator != operator_group) {
                if (!cur->parent) {
                    logerr("Syntax error: Check brackets!");
                    goto out;
                }
                cur = cur->parent;
            }
        }
        if (t->flags & token_rightop) {
            f = calloc(1, sizeof(filter_t));
            if (!f)
                goto out;
            f->parent = cur;
            f->operator = t->operator;
            if (t->flags & token_leftop) {
                if (!cur->op2) {
                    logerr("Syntax error: Binary operator not expected");
                    filter_destroy(f);
                    goto out;
                }
                f->op1 = cur->op2;
            }
            cur->op2 = f;
            cur = f;
        } else if (t->key == token_closebracket) {
            if (!cur->parent) {
                logerr("Syntax error: Check brackets!");
                goto out;
            }
            cur = cur->parent;
            while (cur->operator != operator_group) {
                if (!cur->parent) {
                    logerr("Syntax error: Check brackets!");
                    goto out;
                }
                cur = cur->parent;
            }
        } else if (t->key == token_finish) {
            if (cur != root) {
                logerr("Syntax error: Check brackets!");
                goto out;
            }
        }
    } while (t->key != token_finish);
    if (strcmp(t->string, "]")) {
        logerr("Syntax error: Filter not ]-terminated!");
        goto out;
    }
    r = true;
    *endptr = buf;
out:
    if (!r) {
        filter_destroy(root);
        root = NULL;
    }
    return root;
}

void filter_destroy(filter_t * filter) {
    if (!filter)
        return;
    filter_destroy(filter->op1);
    filter_destroy(filter->op2);
    variant_cleanup(&filter->value);
    regfree(&filter->regvalue);
    free(filter);
}

bool filter_test(filter_t *filter, object_t *object, reqflags_t *flags) {
    bool ret = false;
    int diff = 1;
    variant_t namevalue;
    const variant_t *value;
    char *charvalue;
    if (!filter)
        return true;
    switch (filter->operator) {
        case operator_eq:
        case operator_ne:
        case operator_gte:
        case operator_lte:
        case operator_gt:
        case operator_lt:
            variant_initialize(&namevalue, variant_type_string);
            if (!filter->param) {
                variant_setChar(&namevalue, object_name(object, flags->indexpath?0:path_attr_key_notation));
                value = &namevalue;
            } else if (object_isInstance(object) && !strcmp(filter->param, "key")) {
                variant_setChar(&namevalue, object_name(object, path_attr_key_notation));
                value = &namevalue;
            } else if (object_isInstance(object) && !strcmp(filter->param, "index")) {
                variant_setChar(&namevalue, object_name(object, 0));
                value = &namevalue;
            } else {
                value = parameter_getValue(object_getParameter(object, filter->param));
            }
            variant_compare(value, &filter->value, &diff);
            variant_cleanup(&namevalue);
            return
                filter->operator == operator_eq ? diff == 0 :
                filter->operator == operator_ne ? diff != 0 :
                filter->operator == operator_gte ? diff >= 0 :
                filter->operator == operator_lte ? diff <= 0 :
                filter->operator == operator_gt ? diff > 0 :
                filter->operator == operator_lt ? diff < 0 :
                false;
        case operator_regex:
            if (!filter->param)
                return !regexec(&filter->regvalue, object_name(object, flags->indexpath?0:path_attr_key_notation), 0, NULL, 0);
            else if (object_isInstance(object) && !strcmp(filter->param, "key"))
                return !regexec(&filter->regvalue, object_name(object, path_attr_key_notation), 0, NULL, 0);
            else if (object_isInstance(object) && !strcmp(filter->param, "index"))
                return !regexec(&filter->regvalue, object_name(object, 0), 0, NULL, 0);
            charvalue = variant_char(parameter_getValue(object_getParameter(object, filter->param)));
            if (charvalue) {
                ret = !regexec(&filter->regvalue, charvalue, 0, NULL, 0);
                free(charvalue);
            }
            return ret;
        case operator_flag:
            if (!filter->param)
                return true;
            if (!strcmp(filter->param, "read-only")) {
                ret = object_attributes(object) & object_attr_read_only;
            } else if (!strcmp(filter->param, "template")) {
                ret = object_attributes(object) & object_attr_template;
            } else if (!strcmp(filter->param, "instance")) {
                ret = object_attributes(object) & object_attr_instance;
            } else if (!strcmp(filter->param, "accept-parameters")) {
                ret = object_attributes(object) & object_attr_accept_parameters;
            } else if (!strcmp(filter->param, "persistent")) {
                ret = object_attributes(object) & object_attr_persistent;
            }
            return ret;
        case operator_and:
            return filter_test(filter->op1, object, flags) && filter_test(filter->op2, object, flags);
        case operator_or:
            return filter_test(filter->op1, object, flags) || filter_test(filter->op2, object, flags);
        case operator_not:
            return !filter_test(filter->op2, object, flags);
        case operator_group:
            return filter_test(filter->op2, object, flags);
    }
    return false;
}


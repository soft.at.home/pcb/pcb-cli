/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <components.h>

#include <string.h>
#include <stdio.h>
#include <signal.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <stdbool.h>

#include <debug/sahtrace.h>

#include <pcb/utils.h>
#include <pcb/core.h>
#ifdef CONFIG_SAH_SERVICES_PCB_CLI_USER_CHECK
#include <usermngt/usermngt.h>
#endif

#include "util.h"
#include "env.h"
#include "req.h"
#include "tty.h"

#if defined(__GNUC__) && __GNUC__ >= 7
#define FALLTHROUGH __attribute__ ((fallthrough))
#else
#define FALLTHROUGH
#endif

#ifdef CONFIG_PCB_CLI_STATIC
void libpcb_serialize_ddw_register();
#endif

static const char *usageMessage =
    "Copyright (c) 2010 SoftAtHome\n"
    "\n"
    "Usage: pcb_cli [options] [uri] [commands]\n"
    "\n"
    "If no commands are specified, stdin is processed line by line.\n"
    "And if stdin is a terminal, you get an interactive prompt.\n"
    "\n"
    "Options are:\n"
    "  -n  --name <name>             Set the name of the pcb client\n"
    "  -u  --user <user>             Open a session using this username\n"
    "  -i  --indexpath               Use index path notation i.s.o. key path notation\n"
    "  -s  --slashpath               Use slash path notation i.s.o. dot path notation\n"
    "  -p  --persistent              Mark created instances as persistent\n"
    "  -P  --non-persistent          Mark created instances as non-persistent\n"
    "  -d  --define                  Get requests return definition i.s.o. content\n"
    "      --depth <depth>           Depth of get requests\n"
    "  -z  --zip                     Get requests only return objects (no params...)\n"
    "  -j  --json                    Use JSON notation\n"
    "  -V  --validateonly            Set requests are validated only\n"
    "      --path <path>             Set the reference path\n"
    "  -l  --less                    Print only parameter and return values\n"
    "  -q  --quiet                   Print nothing\n"
    "  -e  --exitonerr               Exit on error\n"
    "  -v  --verbose                 Produce more output\n"
    "      --globalnotifications     Print global notifications\n"
    "  -U  --no_update_notifications Suppress value_changed notifications\n"
    "  -A  --no_add_notifications    Suppress object_added notifications\n"
    "  -D  --no_delete_notifications Suppress object_deleted notifications\n"
    "  -C  --no_custom_notifications Suppress custom notifications\n"
    "  -N  --notifications_only      Do not fetch the initial values\n"
    "  -a  --anonymous               Anonymous requests\n"
    "  -w  --wait <sec>              Wait up to <sec> for object to become available\n"
    "                                and to complete the request\n"
    "  -T  --timeout <sec>           Wait up to <sec> for reply\n"
    "  -t  --template_info           Include template objects in get requests\n"
    "      --daemon                  Daemonize at startup\n"
    "      --pidfile <pidfile>       Write process id to <pidfile>\n"
    "  -Z  --precise_time_stamps     Use precise time stamps for notifications\n"
    "\n"
    "Command syntax\n"
    "\n"
    "Get objects:\n"
    "  Syntax    <path>?[<depth>][{<parameter> [, <parameter>]*}][&|$]\n"
    "  Examples  Foo?\n"
    "            Foo.foo?0{Property1, Property2}&\n"
    "Set parameter:\n"
    "  Syntax    <path>.<parameter>=<value>\n"
    "  Example   Foo.foo.Property=bar\n"
    "Add instance:\n"
    "  Syntax    <path>+[{<parameter=value> [, <parameter=value>]*}]\n"
    "  Examples  Foo+\n"
    "            Foo+{key=foo, index=99, Property1=\"value1\"}\n"
    "Claim object/instance:\n"
    "  Syntax    <path>[{<parameter=value> [, <parameter=value>]*}]\n"
    "  Example   Foo.foo{index=99, Property1=\"value1\"}\n"
    "Delete instance:\n"
    "  Syntax    <path>-\n"
    "  Example   Foo.foo-\n"
    "Function call:\n"
    "  Syntax    <path>.<function>([[argument=]value [, [argument=]value]*])\n"
    "  Examples  Foo.foo.f(bar1, bar2)\n"
    "            Foo.foo.f(arg2=\"bar2\", arg1=\"bar1\")\n"
    "\n"
    "Paths may contain zero or more filters. A filter is an expression between square\n"
    "brackets, e.g. [Property==\"bar\"]. A filter contains of <param><op><value>\n"
    "criteria. Valid ops are ==, !=, >=, <=, >, < and ~. ~ means match a regexp.\n"
    "Several criteria may be combined with the logical operators &&, ||, ! and ().\n"
    "\n"
    "This help message is only a mnemonic. For detailed documentation, go to\n"
    "http://wiki.be.softathome.com/mediawiki/index.php/PCB_CLI\n"
    "\n";

static char rbuf[256];
static char *bufend;
static llist_t conns = {NULL, NULL};

static void signalHandler(int signal) {
    (void)signal;
    connection_setTerminated(true);
    globalenv.exit = 1;
}

static bool processCommand(const char *cmd) {
    slprintf(cmdbuf, sizeof(cmdbuf), "%s", cmd);
    switch (req_parse(cmdbuf, sizeof(cmdbuf))) {
    case parse_success:
        return true;
    case parse_we_want_more:
        logerr("Incomplete command");
        FALLTHROUGH;
    case parse_error:
    default:
        return false;
    }
}

static bool fileHandler(peer_info_t *peer) {
    int fd = peer_getFd(peer);
    int n;
    char *rbufptr, *rbufend;

    n = read(fd, rbuf, sizeof(rbuf));
    if (n <= 0) {
        if (globalenv.fireandstop) {
            while(peer_needWrite(globalenv.peer)) {
                if (!peer_flush(globalenv.peer)) {
                    break;
                }
            }
            globalenv.workload = 0;
        } else {
            globalenv.workload--;
        }
        return false;
    }
    rbufptr = rbuf;
    rbufend = rbuf + n;

    while (rbufptr < rbufend) {
        while (*rbufptr != '\n' && rbufptr < rbufend) {
            if (bufend < cmdbuf + sizeof(cmdbuf) - 1)
                *bufend++ = *rbufptr;
            rbufptr++;
        }
        if (rbufptr < rbufend) {
            *bufend = '\0';
            if (req_parse(cmdbuf, sizeof(cmdbuf)) == parse_error)
                return false;
            bufend = cmdbuf;
            rbufptr++;
        }
    }
    return true;
}

static bool globalNotification(pcb_t *pcb, peer_info_t *from, notification_t *notification) {
    (void)pcb; (void)from;
    if (!globalenv.globalnotifications)
        return true;
    pcb_datetime_t dt;
    pcb_datetime_now(&dt);
    string_t currentTime;
    string_initialize(&currentTime,0);
    string_fromTimeExtended(&currentTime, &dt, globalenv.reqflags.precise_time_stamps ? ISO8601_DATE_TIME_FMT_US : ISO8601_DATE_TIME_FMT);
    string_t s;
    string_initialize(&s, 256);
    notification_details(notification, &s, &globalenv.reqflags);
    output("[%s] notification[%d]%s", string_buffer(&currentTime), notification_type(notification), string_buffer(&s));
    string_cleanup(&s);
    string_cleanup(&currentTime);
    return true;
}

static bool connectionClosed(peer_info_t *peer) {
    (void)peer;
    globalenv.exit = true;

    return true;
}

static bool connectPcb() {

    if (strcmp("pcb_cli", globalenv.name)) {
        sahTraceClose();
        sahTraceOpen(globalenv.name, TRACE_TYPE_SYSLOG);
        sahTraceSetLevel(PCB_CLI_TRACE_LEVEL);
        SAH_TRACE_INFO("Logging started");
    }

    globalenv.input = connection_create("input");
    if (!globalenv.input) {
        logerr("connection_create(name=\"%s\") failed", "input");
        return false;
    }

    uri_t *uri = NULL;
#ifdef CONFIG_PCB_CLI_STATIC
        pcb_register_func_t serializers[3] = { libpcb_serialize_ddw_register, NULL };
        globalenv.pcb = pcb_createStatic(globalenv.name, 0, NULL, serializers);
#else
        globalenv.pcb = pcb_create(globalenv.name, 0, NULL);
#endif
    if (!globalenv.pcb) {
        logerr("pcb_create(name=\"%s\") failed", globalenv.name);
        return false;
    }

    // set notify handler
    if (!pcb_setNotifyHandler(globalenv.pcb, globalNotification)) {
        SAH_TRACE_ERROR("Failed to set notify handler");
        return false;
    }

    connection_setSignalEventHandler(pcb_connection(globalenv.pcb), SIGTERM, signalHandler);
    connection_setSignalEventHandler(pcb_connection(globalenv.pcb), SIGPIPE, NULL);

    if (!*globalenv.uri) {
        const char *pcb_uri = getenv("PCB_SYS_BUS");
        if (pcb_uri && *pcb_uri) {
            snprintf(globalenv.uri, sizeof(globalenv.uri), "%s", pcb_uri);
        } else {
            const char *filename = getenv("PCB_SYSTEM");
            if (!filename || !*filename)
                filename = "/var/run/pcb_sys";
            snprintf(globalenv.uri, sizeof(globalenv.uri), "pcb://ipc:{%s}", filename);
        }
    }

    globalenv.peer = connection_connect(pcb_connection(globalenv.pcb), globalenv.uri, &uri);
    if (!globalenv.peer) {
        logerr("Could not connect to %s", globalenv.uri);
        uri_destroy(uri);
        return false;
    }
    uri_destroy(uri);

    peer_setEventHandler(globalenv.peer,peer_event_close,0,connectionClosed);

    llist_append(&conns, connection_getIterator(pcb_connection(globalenv.pcb)));
    llist_append(&conns, connection_getIterator(globalenv.input));

    if (!*globalenv.user)
        return true;

    request_t *request = request_create_openSession(globalenv.user);
    if (!request)
        goto opensession_error;
    if (!pcb_sendRequest(globalenv.pcb, globalenv.peer, request))
        goto opensession_error;
    if (!pcb_waitForReply(globalenv.pcb, request, NULL))
        goto opensession_error;
    reply_t *reply = request_reply(request);
    if (!reply)
        goto opensession_error;
    reply_item_t *item;
    for (item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
        if (reply_item_type(item) == reply_type_error)
            goto opensession_error;
    }

    request_destroy(request);
    return true;

opensession_error:
    logerr("Could not open session (username=%s)", globalenv.user);
    request_destroy(request);
    return false;
}

int main(int argc, char **argv, char **envp) {
    peer_info_t *peer;
    char *paramset;
    char *param;
    char *value;
    FILE * pidfile;
    int ret = -1;
    int commandsProcessed = 0;

    sahTraceOpen("pcb_cli", TRACE_TYPE_SYSLOG);
    sahTraceSetLevel(PCB_CLI_TRACE_LEVEL);
    SAH_TRACE_INFO("Logging started");

    if (!connection_initLibrary()) {
        logerr("connection_initLibrary failed.");
        goto leave;
    }

    for (; *envp; envp++) {
        if (strncasecmp(*envp, "pcb_cli_", 8))
            continue;
        param = *envp + 8;
        value = strchr(*envp, '=');
        *value++ = '\0';
        setEnv(param, value, 1);
    }

#ifdef CONFIG_SAH_SERVICES_PCB_CLI_USER_CHECK
    uid_t uid = getuid();
    if (uid != 0) {
        const usermngt_user_t *user = usermngt_userFindByID((uint32_t) uid);
        if (user == NULL) {
            logerr("No PCB user for uid %d", uid);
            goto leave;
        }
        if (!setEnv("user", usermngt_userName(user), 0)) {
            goto leave;
        }
    }
#endif

    ++argv;
    --argc;
    while (argc) {
        if (!strcmp(*argv, "-h") || !strcmp(*argv, "--help")) {
            usrinf("%s", usageMessage);
            goto leave;
        } else if (!strncmp(*argv, "--", 2)) {
            param = *argv + 2;
            if (   !strcmp(param, "name") ||
                   !strcmp(param, "depth") ||
                   !strcmp(param, "path") ||
                   !strcmp(param, "pidfile") ||
                   !strcmp(param, "wait") ||
                   !strcmp(param, "timeout") ||
                   !strcmp(param, "user")) {
                ++argv;
                --argc;
                if (!argc) {
                    usrinf("%s", usageMessage);
                    goto leave;
                }
                value = *argv;
            } else {
                value = "yes";
            }
            if (!setEnv(param, value, 0)) {
                goto leave;
            }
        } else if (**argv == '-') {
            for (paramset = *argv + 1; *paramset; paramset++) {
                if (*paramset == 'n' || *paramset == 'u' || *paramset == 'w' || *paramset == 'T') {
                    ++argv;
                    --argc;
                    if (!argc) {
                        usrinf("%s", usageMessage);
                        goto leave;
                    }
                    value = *argv;
                } else {
                    value = "yes";
                }
                switch (*paramset) {
                    case 'n': param = "name"; break;
                    case 'u': param = "user"; break;
                    case 'i': param = "indexpath"; break;
                    case 's': param = "slashpath"; break;
                    case 'd': param = "define"; break;
                    case 'z': param = "zip"; break;
                    case 'p': param = "persistent"; break;
                    case 'P': param = "non_persistent"; break;
                    case 'l': param = "less"; break;
                    case 'q': param = "quiet"; break;
                    case 'j': param = "json"; break;
                    case 'V': param = "validateonly"; break;
                    case 'e': param = "exitonerr"; break;
                    case 'f': param = "fireandstop"; break;
                    case 'F': param = "fireandforget"; break;
                    case 'v': param = "verbose"; break;
                    case 'U': param = "no_update_notifications"; break;
                    case 'A': param = "no_add_notifications"; break;
                    case 'D': param = "no_delete_notifications"; break;
                    case 'C': param = "no_custom_notifications"; break;
                    case 'T': param = "timeout"; break;
                    case 'w': param = "wait"; break;
                    case 'N': param = "notifications_only"; break;
                    case 'a': param = "anonymous"; break;
                    case 't': param = "template_info"; break;
                    case 'Z': param = "precise_time_stamps"; break;
                    default: param = paramset; *(paramset+1) = '\0';
                }
#ifdef CONFIG_SAH_SERVICES_PCB_CLI_USER_CHECK
                if (strncmp(param, "user", sizeof("user")) == 0 && getuid() != 0) {
                    logerr("Only root can use the -u option");
                    goto leave;
                }
#endif
                if (!setEnv(param, value, 0))
                    goto leave;
            }
        } else if (!strncmp(*argv, "pcb://", 6)) {
            if (!setEnv("uri", *argv, 0))
                goto leave;
            if (!connectPcb())
                goto leave;
        } else {
            if (!globalenv.pcb) {
                if (!connectPcb())
                    goto leave;
            }
            if (!processCommand(*argv))
                goto leave;
            commandsProcessed++;
        }
        ++argv;
        --argc;
    }

    if (!commandsProcessed) {
        if (!globalenv.pcb) {
            if (!connectPcb())
                goto leave;
        }
        if (isatty(fileno(stdin)) && !globalenv.daemon) {
            globalenv.interactive = 1;
            tty_open();
        } else {
            bufend = cmdbuf;
            peer = connection_addCustom(globalenv.input, fileno(stdin));
            peer_setEventHandler(peer, peer_event_read, peer_handler_read_always, fileHandler);
            globalenv.workload++;
        }
    } else {
        if (globalenv.fireandstop) {
            while(peer_needWrite(globalenv.peer)) {
                if (!peer_flush(globalenv.peer)) {
                    break;
                }
            }
            goto leave;
        }
    }

    if (globalenv.daemon) {
        if (daemon(1, 1)) {
            logerr("daemon failed");
            goto leave;
        }
    }

    if (*globalenv.pidfile) {
        pidfile = fopen(globalenv.pidfile, "w");
        if (pidfile) {
            fprintf(pidfile, "%u\n", getpid());
            fclose(pidfile);
        } else {
            logerr("Can't write to pidfile %s", *globalenv.pidfile);
        }
    }

    fd_set readset;
    fd_set writeset;

    while (!globalenv.exit && globalenv.workload > 0) {
        ret = connection_waitForEvents_r(&conns, NULL, &readset, &writeset);
        if (ret < 0) { // error
            break;
        } else if (ret > 0) {
            connection_handleEvents_r(&conns,&readset,&writeset);
        }
    }

    ret = globalenv.exit;

leave:
    if (*globalenv.pidfile)
        unlink(globalenv.pidfile);

    req_close();

    if (globalenv.interactive)
        tty_close();

    llist_cleanup(&conns);

    if (globalenv.input)
        connection_destroy(globalenv.input);

    if (globalenv.pcb)
        pcb_destroy(globalenv.pcb);

    SAH_TRACE_INFO("cleanup pcb library");
    connection_exitLibrary();

    SAH_TRACE_INFO("Exit");
    sahTraceClose();

    return ret;
}


/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <termios.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <signal.h>

#include <components.h>

#include <pcb/utils/linked_list.h>
#include <debug/sahtrace.h>

#include "util.h"
#include "tab.h"
#include "env.h"
#include "req.h"
#include "tty.h"

typedef enum _control_key {
    control_key_backspace,
    control_key_delete,
    control_key_left,
    control_key_right,
    control_key_home,
    control_key_end,
    control_key_up,
    control_key_down,
    control_key_tab,
    control_key_newline,
    control_key_eof,
    control_key_backword,
    control_key_backline,
    control_key_delline,
} control_key_t;

typedef struct _control_key_sequence {
    char seq[10];
    int seqlen;
    control_key_t key;
} control_key_sequence_t;

typedef struct _history history_t;

struct _history {
    history_t *prv;
    history_t *nxt;
    char *cmd;
};

static int ttyfd;
static peer_info_t *ttypeer;
static struct termios origTermios;
static char rbuf[256];
static char *rbufptr;
static char *rbufend;
static char wbuf[256];
static char *wbufend;
static char wbuftmp[256];
static char prompt[256];
static int promptlen;
static int tabcount = 0;
static int sigintcount = 0;

static control_key_sequence_t control_key_sequence[] = {
    {{127}             , 1, control_key_backspace},
    {{8}               , 1, control_key_backspace},
    {{27, 91, 51, 126} , 4, control_key_delete   },
    {{2}               , 1, control_key_left     },
    {{27, 91, 68}      , 3, control_key_left     },
    {{27, 79, 68}      , 3, control_key_left     },
    {{6}               , 1, control_key_right    },
    {{27, 91, 67}      , 3, control_key_right    },
    {{27, 79, 67}      , 3, control_key_right    },
    {{1}               , 1, control_key_home     },
    {{27, 79, 72}      , 3, control_key_home     },
    {{27, 91, 49, 126} , 4, control_key_home     },
    {{5}               , 1, control_key_end      },
    {{27, 79, 70}      , 3, control_key_end      },
    {{27, 91, 65}      , 3, control_key_up       },
    {{27, 79, 65}      , 3, control_key_up       },
    {{16}              , 1, control_key_up       },
    {{14}              , 1, control_key_down     },
    {{27, 91, 66}      , 3, control_key_down     },
    {{27, 79, 66}      , 3, control_key_down     },
    {{9}               , 1, control_key_tab      },
    {{10}              , 1, control_key_newline  },
    {{4}               , 1, control_key_eof      },
    {{23}              , 1, control_key_backword },
    {{21}              , 1, control_key_backline },
    {{11}              , 1, control_key_delline  },
};
static int control_key_sequence_len = sizeof(control_key_sequence) / sizeof(control_key_sequence_t);

static history_t history = {&history, &history, NULL};
static int history_len = 1;
static const int history_limit = 100;
static history_t *history_cur = &history;

static int processingInput = 0;

static char *buf = cmdbuf;
static int buflen = sizeof(cmdbuf);
static char *bufend , *bufcur, *bufdirty, *bufcurprv, *bufendprv, *buftab, *buftmp;

static const char *stopCharacters = ".,/(){}[]= ";
static const char *terminalHelp =
    "Command syntax\n"
    "\n"
    "Get objects:\n"
    "  Syntax    <path>?[<depth>][{<parameter> [, <parameter>]*}][&|$]\n"
    "  Examples  Foo?\n"
    "            Foo.foo?0{Property1, Property2}&\n"
    "Set parameter:\n"
    "  Syntax    <path>.<parameter>=<value>\n"
    "  Example   Foo.foo.Property=bar\n"
    "Add instance:\n"
    "  Syntax    <path>+[{<parameter=value> [, <parameter=value>]*}]\n"
    "  Examples  Foo+\n"
    "            Foo+{key=foo, index=99, Property1=\"value1\"}\n"
    "Claim object/instance:\n"
    "  Syntax    <path>[{<parameter=value> [, <parameter=value>]*}]\n"
    "  Example   Foo.foo{index=99, Property1=\"value1\"}\n"
    "Delete instance:\n"
    "  Syntax    <path>-\n"
    "  Example   Foo.foo-\n"
    "Function call:\n"
    "  Syntax    <path>.<function>([[argument=]value [, [argument=]value]*])\n"
    "  Examples  Foo.foo.f(bar1, bar2)\n"
    "            Foo.foo.f(arg2=\"bar2\", arg1=\"bar1\")\n"
    "\n"
    "Paths may contain zero or more filters. A filter is an expression between square\n"
    "brackets, e.g. [Property==\"bar\"]. A filter contains of <param><op><value>\n"
    "criteria. Valid ops are ==, !=, >=, <=, >, < and ~. ~ means match a regexp.\n"
    "Several criteria may be combined with the logical operators &&, ||, ! and ().\n"
    "\n"
    "Special commands\n"
    "  <option>=<value> Possible options are:\n"
    "    indexpath=y|n               Use index path notation i.s.o. key path notation\n"
    "    slashpath=y|n               Use slash path notation i.s.o. dot path notation\n"
    "    persistent=y|n              Mark created instances as persistent\n"
    "    non_persistent=y|n          Mark created instances as non-persistent\n"
    "    define=y|n                  Get requests return definition i.s.o. content\n"
    "    depth=<depth>               Depth of get requests\n"
    "    zip=y|n                     Get requests only return objects (no params...)\n"
    "    path=<path>                 Set the reference path\n"
    "    less=y|n                    Print only parameter and return values\n"
    "    quiet=y|n                   Print nothing\n"
    "    json=y|n                    Use JSON notation\n"
    "    validateonly=y|n            Set requests are validated only\n"
    "    exitonerr=y|n               Exit on error\n"
    "    globalnotifications=y|n     Print global notifications\n"
    "    no_update_notifications=y|n Suppress value_changed notifications\n"
    "    no_add_notifications=y|n    Suppress object_added notifications\n"
    "    no_delete_notifications=y|n Suppress object_deleted notifications\n"
    "    no_custom_notifications=y|n Suppress custom notifications\n"
    "    wait=<sec>                  Wait up to <sec> for object to become available\n"
    "                                    and to complete the request\n"
    "    template_info=y|n           Include template object in get requests\n"
    "    precise_time_stamps=y|n     Make time stamps more precise (microseconds)\n"
    "  cd               Synonym for path=\n"
    "  cd <path>        Synonym for path=<current path>.<path>\n"
    "  cd ..            Synonym for path=<parent of current path>\n"
    "  ls               Synonym for ?0\n"
    "  help             Displays a help message\n"
    "  exit             Exit\n"
    "  lsreq            Lists all open notification requests\n"
    "  printenv         Print all options\n"
    "\n"
    "This help message is only a mnemonic. For detailed documentation, go to\n"
    "http://wiki.be.softathome.com/mediawiki/index.php/PCB_CLI\n"
    "\n";

#ifdef CONFIG_SAH_SERVICES_PCB_CLI_SSH_LOG_CMD
    static bool is_SSH_session=false;
    static char *ssh_src_ip = "undefined";
    static unsigned long ssh_src_port=0;
#endif

static int history_move(char *current_buffer, int bufsize, int step) {
    if (step > 0 && history_cur == &history)
        return strlen(current_buffer);
    if (step < 0 && history_cur->prv == &history)
        return strlen(current_buffer);
    if (history_cur->cmd)
        free(history_cur->cmd);
    history_cur->cmd = (char *)malloc(strlen(current_buffer) + 1);
    if (history_cur->cmd)
        strcpy(history_cur->cmd, current_buffer);
    if (step > 0) {
        do {
            history_cur = history_cur->nxt;
            step--;
        } while (step > 0);
    } else if (step < 0) {
        do {
            history_cur = history_cur->prv;
            step++;
        } while (step < 0);
    }
    return slprintf(current_buffer, bufsize, "%s", history_cur->cmd?history_cur->cmd:"");
}

static void history_push(char *cmd) {
    while (history_len >= history_limit) {
        history_cur = history.nxt;
        if (history_cur->cmd)
            free(history_cur->cmd);
        history_cur->nxt->prv = history_cur->prv;
        history_cur->prv->nxt = history_cur->nxt;
        free(history_cur);
        history_len--;
    }
    history_cur = (history_t *)malloc(sizeof(history_t));
    if (history_cur)
        history_cur->cmd = (char *)malloc(strlen(cmd)+1);
    if (history_cur && history_cur->cmd) {
        strcpy(history_cur->cmd, cmd);
        history_cur->prv = history.prv;
        history_cur->nxt = &history;
        history_cur->prv->nxt = history_cur;
        history_cur->nxt->prv = history_cur;
        history_len++;
    }
}


static void flushWriteBuffer() {
    if (wbufend > wbuf) {
        ssize_t ssize = write(STDERR_FILENO, wbuf, wbufend - wbuf);
        (void)ssize; // avoid ignore write return value warnings. In practice it works fair enough and it's not critical
        wbufend = wbuf;
    }
}

static void bufferedWrite(const char *fmt, ...) {
    va_list arg;
    va_start(arg, fmt);
    int n = vsnprintf(wbuftmp, sizeof(wbuftmp), fmt, arg);
    va_end(arg);
    if (n > (int)sizeof(wbuf) - (wbufend - wbuf))
        flushWriteBuffer();
    if (n > (int)sizeof(wbuf))
        n = sizeof(wbuf);
    memcpy(wbufend, wbuftmp, n);
    wbufend += n;
}

static void wrappedWrite(const char *s, int cols, int cur) {
    flushWriteBuffer();
    int n = cols - cur;
    while (*s) {
        n = slprintf(wbufend, n + 1, "%s", s);
        wbufend += n;
        s += n;
        if (cur + n == cols)
            *wbufend++ = '\n';
        cur = 0;
        flushWriteBuffer();
        n = cols;
    }
}

static int colCount() {
    struct winsize termsize;
    ioctl(ttyfd, TIOCGWINSZ, &termsize);
    if (termsize.ws_col == 0)
        return 80;
    else if (termsize.ws_col > sizeof(wbuf) - 1)
        return sizeof(wbuf) - 1;
    else
        return termsize.ws_col;
}

static void makePrompt() {
    promptlen = slprintf(prompt, sizeof(prompt), "%s> ", globalenv.path);
}

static void move(int width, int cur, int target) {
    int rows = target / width - cur / width;
    int cols = target % width - cur % width;
    if (rows > 0)
        bufferedWrite("\033[%dB", rows);
    else if (rows < 0)
        bufferedWrite("\033[%dA", -rows);
    if (cols > 0)
        bufferedWrite("\033[%dC", cols);
    else if (cols < 0)
        bufferedWrite("\033[%dD", -cols);
    flushWriteBuffer();
}

static void backspace(int width, int cur, int target) {
    int rows = target / width - cur / width;
    int cols = target % width - cur % width;
    int row;
    for (row=0; row>rows; row--)
        bufferedWrite("\033[2K\033[A");
    if (cols > 0)
        bufferedWrite("\033[%dC", cols);
    else if (cols < 0)
        bufferedWrite("\033[%dD", -cols);
    bufferedWrite("\033[K");
    flushWriteBuffer();
}

static void complete(const char *completion) {
    if (buftab < buf || buftab > bufcur)
        return;
    int cols = colCount();
    int n = strlen(completion);
    slinsert(buftab, buflen - (buftab - buf), completion);
    tabcount = 0;
    if (bufdirty > buftab)
        bufdirty = buftab;
    bufend += n;
    bufcur += n;
    *bufend = '\0';
    move(cols, promptlen + (bufcurprv - buf), promptlen + (bufendprv - buf));
    backspace(cols, promptlen + (bufendprv - buf), promptlen + (bufdirty - buf));
    wrappedWrite(bufdirty, cols, (promptlen + (bufdirty - buf)) % cols);
    move(cols, promptlen + (bufend - buf), promptlen + (bufcur - buf));

    bufdirty = bufend;
    bufcurprv = bufcur;
    bufendprv = bufend;
}

static bool tty_done() {
    static bool done = false;
    if (done)
        return false;
    done = true;
    beforeOutput = NULL;
    afterOutput = NULL;
    ssize_t ssize = write(STDERR_FILENO, "\n", 1);
    (void)ssize; // avoid ignore write return value warnings. In practice it works fair enough and it's not critical
    tcsetattr(ttyfd, TCSANOW, &origTermios);
    processingInput = 0;
    peer_destroy(ttypeer);
    ttypeer = NULL;
    globalenv.workload--;
    return false;
}

static bool tty_handler(peer_info_t *peer) {
    (void)peer;
    int n, yield=0, cols;
    control_key_sequence_t key = {{}, 0, control_key_eof};
    char tabTmp;
    ssize_t ssize;

    n = read(ttyfd, rbuf, sizeof(rbuf));
    if (n <= 0 || (n == 1 && *rbuf == 4/*= EOT = Ctrl+D*/))
        return tty_done();

    sigintcount = 0;

    rbufptr = rbuf;
    rbufend = rbuf + n;

    cols = colCount();

    while (rbufptr < rbufend) {
        yield = 0;
        while (!yield && rbufptr < rbufend) {
            key.seq[key.seqlen++] = *rbufptr;
            for (n = 0; n < control_key_sequence_len; n++) {
                if (!memcmp(control_key_sequence[n].seq, key.seq, key.seqlen))
                    break;
            }
            if (n == control_key_sequence_len) {
                tabcount = 0;
                for (n = 0; n < key.seqlen; n++) {
                    if (key.seq[n] < 32)
                        continue;
                    if (bufend > bufcur && bufend + 2 < buf + buflen)
                        memmove(bufcur + 1, bufcur, bufend - bufcur);
                    *bufcur = *rbufptr;
                    if (bufdirty > bufcur)
                        bufdirty = bufcur;
                    if (bufend + 1 < buf + buflen)
                        bufend++;
                    if (bufcur + 1 < buf + buflen)
                        bufcur++;
                }
                key.seqlen = 0;
            } else if (key.seqlen == control_key_sequence[n].seqlen) {
                if (control_key_sequence[n].key != control_key_tab) {
                    tab_cancel();
                    tabcount = 0;
                }
                switch (control_key_sequence[n].key) {
                case control_key_backword:
                    if (bufcur > buf) {
                        buftmp = bufcur - 1;
                        while (buftmp > buf && !strchr(stopCharacters, *(buftmp - 1)))
                            buftmp--;
                        memmove(buftmp, bufcur, bufend - bufcur);
                        bufend -= bufcur - buftmp;
                        bufcur = buftmp;
                        if (bufdirty > bufcur)
                            bufdirty = bufcur;
                        buftmp = NULL;
                    }
                    break;
                case control_key_backline:
                    bufdirty = bufend = bufcur = buf;
                    break;
                case control_key_delline:
                    bufdirty = bufend = bufcur;
                    break;
                case control_key_backspace:
                    if (bufcur > buf) {
                        if (bufend > bufcur)
                            memmove(bufcur - 1, bufcur, bufend - bufcur);
                        bufcur--; bufend--;
                        if (bufdirty > bufcur)
                            bufdirty = bufcur;
                    }
                    break;
                case control_key_delete:
                    if (bufcur < bufend) {
                        memmove(bufcur, bufcur + 1, bufend - bufcur);
                        bufend--;
                        if (bufdirty > bufcur)
                            bufdirty = bufcur;
                    }
                    break;
                case control_key_left:
                    if (bufcur > buf)
                        bufcur--;
                    break;
                case control_key_right:
                    if (bufcur < bufend)
                        bufcur++;
                    break;
                case control_key_home:
                    bufcur = buf;
                    break;
                case control_key_end:
                    bufcur = bufend;
                    break;
                case control_key_up:
                    bufcur = bufend = buf + history_move(buf, buflen, -1);
                    bufdirty = buf;
                    break;
                case control_key_down:
                    bufcur = bufend = buf + history_move(buf, buflen, 1);
                    bufdirty = buf;
                    break;
                case control_key_tab:
                    tabcount++;
                    if (tabcount == 1) {
                        buftab = bufcur;
                        tabTmp = *bufcur;
                        *bufcur = '\0';
                        tab_single(buf, complete);
                        *bufcur = tabTmp;
                    } else if (tabcount == 2) {
                        tabTmp = *bufcur;
                        *bufcur = '\0';
                        bufend = bufcur;
                        if (bufdirty) {
                            move(cols, promptlen + (bufcurprv - buf), promptlen + (bufendprv - buf));
                            ssize = write(STDERR_FILENO, "\n", 1);
                            (void)ssize; // avoid ignore write return value warnings. In practice it works fair enough and it's not critical
                        }
                        tab_double(buf);
                        *bufcur = tabTmp;
                        bufdirty = NULL;
                        tabcount = 0;
                    }
                    break;
                case control_key_newline:
                    yield = 1;
                    break;
                case control_key_eof:
                    return false;
                }
                key.seqlen = 0;
            } else if (rbufptr+1 == rbufend) { //incomplete control sequence
                n = read(ttyfd, rbufend, sizeof(rbuf)-(rbufend-rbuf));
                if (n <= 0)
                    return tty_done();
                rbufend += n;
                cols = colCount();
            }
            rbufptr++;
        }

        *bufend = '\0';

        if (bufdirty == NULL) {
            wrappedWrite(prompt, cols, 0);
            bufdirty = buf;
        } else {
            move(cols, promptlen + (bufcurprv - buf), promptlen + (bufendprv - buf));
            backspace(cols, promptlen + (bufendprv - buf), promptlen + (bufdirty - buf));
        }

        wrappedWrite(bufdirty, cols, (promptlen + (bufdirty - buf)) % cols);

        if (!yield) {
            move(cols, promptlen + (bufend - buf), promptlen + (bufcur - buf));

            bufdirty = bufend;
            bufcurprv = bufcur;
            bufendprv = bufend;
        } else {

            processingInput = 0;

            ssize = write(STDERR_FILENO, "\n", 1);
            (void)ssize; // avoid ignore write return value warnings. In practice it works fair enough and it's not critical
            for (bufcur = buf; *bufcur == ' '; bufcur++);
#ifdef CONFIG_SAH_SERVICES_PCB_CLI_SSH_LOG_CMD
            if (is_SSH_session && (strlen(bufcur) != 0)) {
                SAH_TRACEZ_INFO("shlogger", "[%s:%ld] \"%s\" ", ssh_src_ip, ssh_src_port, bufcur);
            }
#endif
            // update history
            if (bufend > bufcur && (!history.prv->cmd || strcmp(history.prv->cmd, bufcur)))
                history_push(bufcur);
            history_cur = &history;
            if (history_cur->cmd) {
                free(history_cur->cmd);
                history_cur->cmd = NULL;
            }

            // check for built-in cmds
            if (!strncmp(bufcur, "exit", 4) && (!bufcur[4] || strchr(" \t\n", bufcur[4]))) {
                globalenv.exit = 1;
                return true;
            } else if (!strncmp(bufcur, "help", 4) && (!bufcur[4] || strchr(" \t\n", bufcur[4]))) {
                usrinf("%s", terminalHelp);
            } else if (!strncmp(bufcur, "ls", 2) && (!bufcur[2] || strchr(" \t\n", bufcur[2]))) {
                slprintf(buf, buflen, "?0");
                req_parse(buf, buflen);
            } else if (!strncmp(bufcur, "cd", 2) && (!bufcur[2] || strchr(" \t\n", bufcur[2]))) {
                bufcur += 2;
                while (*bufcur == ' ' || *bufcur == '\t')
                    bufcur++;
                if (!*bufcur || *bufcur == '\n') {
                    slprintf(buf, buflen, "path=");
                    req_parse(buf, buflen);
                } else if (!strncmp(bufcur, "..", 2)) {
                    slprintf(buf, buflen, "path=%s", globalenv.path);
                    bufcur = strrchr(buf, globalenv.reqflags.slashpath?'/':'.');
                    if (bufcur) {
                        *bufcur = '\0';
                    } else {
                        slprintf(buf, buflen, "path=");
                    }
                    req_parse(buf, buflen);
                } else {
                    if (*bufcur != (globalenv.reqflags.slashpath?'/':'.') && *globalenv.path)
                        slinsert(bufcur, buflen - (bufcur - buf), globalenv.reqflags.slashpath?"/":".");
                    slinsert(bufcur, buflen - (bufcur - buf), globalenv.path);
                    slinsert(bufcur, buflen - (bufcur - buf), "path=");
                    req_parse(bufcur, buflen - (bufcur - buf));
                }
            } else if (!strcmp(bufcur, "printenv")) {
                usrinf("uri=%s", globalenv.uri);
                usrinf("name=%s", globalenv.name);
                usrinf("user=%s", globalenv.user);
                usrinf("indexpath=%s", globalenv.reqflags.indexpath?"yes":"no");
                usrinf("alias=%s", globalenv.reqflags.alias?"yes":"no");
                usrinf("slashpath=%s", globalenv.reqflags.slashpath?"yes":"no");
                usrinf("persistent=%s", globalenv.reqflags.persistent?"yes":"no");
                usrinf("non_persistent=%s", globalenv.reqflags.non_persistent?"yes":"no");
                usrinf("define=%s", globalenv.reqflags.define?"yes":"no");
                usrinf("zip=%s", globalenv.reqflags.zip?"yes":"no");
                usrinf("depth=%d", globalenv.depth);
                usrinf("path=%s", globalenv.path);
                usrinf("less=%s", globalenv.reqflags.less?"yes":"no");
                usrinf("quiet=%s", globalenv.reqflags.quiet?"yes":"no");
                usrinf("json=%s", globalenv.reqflags.json?"yes":"no");
                usrinf("validateonly=%s", globalenv.reqflags.validateonly?"yes":"no");
                usrinf("exitonerr=%s", globalenv.exitonerr?"yes":"no");
                usrinf("globalnotifications=%s", globalenv.globalnotifications?"yes":"no");
                usrinf("no_update_notifications=%s", globalenv.reqflags.no_update_notifications?"yes":"no");
                usrinf("no_add_notifications=%s", globalenv.reqflags.no_add_notifications?"yes":"no");
                usrinf("no_delete_notifications=%s", globalenv.reqflags.no_delete_notifications?"yes":"no");
                usrinf("no_custom_notifications=%s", globalenv.reqflags.no_custom_notifications?"yes":"no");
                usrinf("notifications_only=%s", globalenv.reqflags.notifications_only?"yes":"no");
                usrinf("no_object_updates=%s", globalenv.reqflags.no_object_updates?"yes":"no");
                usrinf("anonymous=%s", globalenv.reqflags.untrusted?"yes":"no");
                usrinf("timeout=%d", globalenv.request_timeout);
                usrinf("wait=%s", globalenv.reqflags.wait?"yes":"no");
                usrinf("template_info=%d", globalenv.reqflags.template_info);
                usrinf("precise_time_stamps=%s", globalenv.reqflags.precise_time_stamps?"yes":"no");
                usrinf("shellify=%s", globalenv.reqflags.shellify?"yes":"no");
                usrinf("uglify=%s", globalenv.reqflags.uglify?"yes":"no");
                usrinf("fireandforget=%s", globalenv.fireandforget?"yes":"no");
#if !defined(CONFIG_PCB_CLI_STATIC) && defined(CONFIG_PCB_HTTP_SERIALIZER)
                usrinf("rpc_ws_output=%s", globalenv.rpc_ws_output?"yes":"no");
                usrinf("ws_version=%d", globalenv.ws_version);
#endif
            } else if (!strcmp(bufcur, "lsreq")) {
                req_list();
            } else if (!strncmp(bufcur, "clreq",5)) {
                strtok(bufcur," ");
                char *id = strtok(NULL," ");
                req_close_pending(atoi(id));
            } else { // no built-in cmd -> process as is
                req_parse(buf, buflen);
            }

            processingInput = 1;
            bufend = bufcur = bufdirty = bufcurprv = bufendprv = buftab = buf;
            *buf = '\0';

            makePrompt();
            wrappedWrite(prompt, cols, 0);
        }
    }
    return true;
}

static void sigintHandler(int signal) {
    (void)signal;
    ssize_t ssize;
    int cols = colCount();
    sigintcount++;
    switch (sigintcount) {
    case 1:
        ssize = write(STDERR_FILENO, "\n", 1);
        (void)ssize; // avoid ignore write return value warnings. In practice it works fair enough and it's not critical
        bufend = bufcur = bufdirty = bufcurprv = bufendprv = buftab = buf;
        *buf = '\0';
        makePrompt();
        wrappedWrite(prompt, cols, 0);
        break;
    case 2:
        connection_setTerminated(true);
        globalenv.exit = 1;
        break;
    }
}

static void ttyBeforeOutput() {
    int cols = colCount();
    switch (processingInput) {
    case 1:
    case 5:
        move(cols, promptlen + (bufcur - buf), promptlen + (bufend - buf));
        backspace(cols, promptlen + (bufend - buf), 0);
        processingInput = 5;
        break;
    default:
        break;
    }
}

static void ttyAfterOutput() {
    int cols = colCount();
    switch (processingInput) {
    case 5:
        wrappedWrite(prompt, cols, 0);
        wrappedWrite(buf, cols, promptlen % cols);
        move(cols, promptlen + (bufend - buf), promptlen + (bufcur - buf));
        break;
    default:
        break;
    }
}

void tty_open(void) {
    usrinf("Copyright (c) 2010 SoftAtHome");
    usrinf("");
    usrinf("Connected to %s", globalenv.uri);
    if (*globalenv.user)
        usrinf("Logged in as %s", globalenv.user);

    struct termios termios;

#ifdef CONFIG_SAH_SERVICES_PCB_CLI_SSH_LOG_CMD
    char *ssh_client_envvar;
    ssh_client_envvar = getenv("SSH_CONNECTION");
    if (ssh_client_envvar && *ssh_client_envvar) {
        is_SSH_session=true;
        sahTraceAddZone(TRACE_LEVEL_INFO, "shlogger");
        char *espace = strchr(ssh_client_envvar, ' ');
        if (espace) {
            ssh_src_ip = strtok(ssh_client_envvar, " ");
            const char *port = strtok(NULL, " ");
            ssh_src_port=atoi(port);
        }
    }
#endif

    // restore history
    FILE *f = fopen("/tmp/pcb_cli_history.log", "r");
    char *bufptr;
    if (f) {
        while (fgets(cmdbuf, sizeof(cmdbuf), f)) {
            bufptr = strchr(cmdbuf, '\n');
            if (bufptr)
                *bufptr = '\0';
            history_push(cmdbuf);
        }
        fclose(f);
    }
    history_cur = &history;

    ttyfd = fileno(stdin);
    rbufptr = rbufend = rbuf;
    wbufend = wbuf;
    tcgetattr(ttyfd, &termios);
    memcpy(&origTermios, &termios, sizeof(struct termios));
    termios.c_lflag &= ~(ECHO | ICANON);
    tcsetattr(ttyfd, TCSANOW, &termios);

    ttypeer = connection_addCustom(globalenv.input, ttyfd);
    peer_setEventHandler(ttypeer, peer_event_read, peer_handler_read_always, tty_handler);
    globalenv.workload++;

    beforeOutput = ttyBeforeOutput;
    afterOutput = ttyAfterOutput;
    bufend = bufcur = bufdirty = bufcurprv = bufendprv = buftab = buf;
    processingInput = 1;
    makePrompt();
    wrappedWrite(prompt, colCount(), 0);

    connection_setSignalEventHandler(pcb_connection(globalenv.pcb), SIGINT, sigintHandler);
}

void tty_close(void) {
    tty_done();
    // backup and free history
    FILE *f = fopen("/tmp/pcb_cli_history.log", "w");
    while ((history_cur = history.nxt) != &history) {
        if (f)
            fprintf(f, "%s\n", history_cur->cmd);
        history_cur->nxt->prv = history_cur->prv;
        history_cur->prv->nxt = history_cur->nxt;
        if (history_cur->cmd)
            free(history_cur->cmd);
        free(history_cur);
    }
    if (f)
        fclose(f);
    history_len = 1;
}


/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <unistd.h>

#include <components.h>

#include <debug/sahtrace.h>

#include "env.h"

env_t globalenv = {
    /* uri         = */ "",
    /* name        = */ "pcb_cli",
    /* user        = */ "",
    /* reqflags    = */ reqflags_initializer,
    /* depth       = */ -1,
    /* path        = */ "",
    /* interactive = */ false,
    /* exit        = */ false,
    /* exitonerr   = */ false,
    /* fireandstop = */ false,
    /* fireandforget = */ false,
    /* verbose     = */ false,
    /* globalnotifications = */ false,
#if !defined(CONFIG_PCB_CLI_STATIC) && defined(CONFIG_PCB_HTTP_SERIALIZER)
    /* rpc_ws_output = */ false,
    /* ws_version = */ 4,
#endif
    /* workload    = */ 0,
    /* pcb         = */ NULL,
    /* peer        = */ NULL,
    /* daemon      = */ false,
    /* pidfile     = */ "",
    /* request_timeout = */ 0,
    /* input       = */ NULL,
};

char cmdbuf[8192] = "";

void (*beforeOutput)(void) = NULL;
void (*afterOutput)(void) = NULL;

static char logbuf[256];

static int readyForOutput = 0;

void beginOutput() {
    if (!readyForOutput++ && beforeOutput)
        beforeOutput();
}

void endOutput() {
    if (!--readyForOutput) {
        if (afterOutput)
            afterOutput();
        fflush(stdout);
    }
}

void output(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    beginOutput();
    vfprintf(stdout, fmt, args);
    fprintf(stdout, "\n");
    endOutput();
    va_end(args);
}

void usrinf(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    beginOutput();
    vfprintf(stderr, fmt, args);
    fprintf(stderr, "\n");
    endOutput();
    va_end(args);
}

void logerr(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vslprintf(logbuf, sizeof(logbuf), fmt, args);
    va_end(args);
    SAH_TRACE_ERROR("%s", logbuf);
    if (globalenv.interactive || globalenv.verbose) {
        beginOutput();
        va_start(args, fmt);
        vfprintf(stderr, fmt, args);
        va_end(args);
        fprintf(stderr, "\n");
        endOutput();
    }
    va_end(args);
}

void loginf(const char * fmt, ...) {
    va_list args;
    va_start(args, fmt);
    if (globalenv.interactive || globalenv.verbose) {
        beginOutput();
        vfprintf(stderr, fmt, args);
        fprintf(stderr, "\n");
        endOutput();
    }
    va_end(args);
}

bool setEnv(const char *param, const char *value, bool ignorecase) {
    int (*cmp)(const char *, const char *) = ignorecase?strcasecmp:strcmp;
    bool valueok = true;
    if (!cmp(param, "uri")) {
        if (globalenv.pcb)
            goto toolate;
        slprintf(globalenv.uri, sizeof(globalenv.uri), "%s", value);
    } else if (!cmp(param, "name")) {
        if (globalenv.pcb)
            goto toolate;
        slprintf(globalenv.name, sizeof(globalenv.name), "%s", value);
    } else if (!cmp(param, "user")) {
        if (globalenv.pcb)
            goto toolate;
        slprintf(globalenv.user, sizeof(globalenv.user), "%s", value);
    } else if (!cmp(param, "fireandstop")) {
        globalenv.fireandstop = parseFlag(&valueok, value);
    } else if (!cmp(param, "fireandforget")) {
        globalenv.fireandforget = parseFlag(&valueok, value);
    } else if (!cmp(param, "indexpath")) {
        globalenv.reqflags.indexpath = parseFlag(&valueok, value);
    } else if (!cmp(param, "alias")) {
        globalenv.reqflags.alias = parseFlag(&valueok, value);
    } else if (!cmp(param, "slashpath")) {
        globalenv.reqflags.slashpath = parseFlag(&valueok, value);
    } else if (!cmp(param, "depth")) {
        globalenv.depth = strtol(value, NULL, 0);
    } else if (!cmp(param, "persistent")) {
        globalenv.reqflags.persistent = parseFlag(&valueok, value);
    } else if (!cmp(param, "non_persistent")) {
        globalenv.reqflags.non_persistent = parseFlag(&valueok, value);
    } else if (!cmp(param, "define")) {
        globalenv.reqflags.define = parseFlag(&valueok, value);
    } else if (!cmp(param, "zip")) {
        globalenv.reqflags.zip = parseFlag(&valueok, value);
    } else if (!cmp(param, "path")) {
        int n = slprintf(globalenv.path, sizeof(globalenv.path), "%s", value);
        if (n > 0 && globalenv.path[n-1] == (globalenv.reqflags.slashpath?'/':'.'))
            globalenv.path[n-1] = '\0';
    } else if (!cmp(param, "less")) {
        globalenv.reqflags.less = parseFlag(&valueok, value);
    } else if (!cmp(param, "quiet")) {
        globalenv.reqflags.quiet = parseFlag(&valueok, value);
    } else if (!cmp(param, "json")) {
        globalenv.reqflags.json = parseFlag(&valueok, value);
    } else if (!cmp(param, "validateonly")) {
        globalenv.reqflags.validateonly = parseFlag(&valueok, value);
    } else if (!cmp(param, "exitonerr")) {
        globalenv.exitonerr = parseFlag(&valueok, value);
    } else if (!cmp(param, "verbose")) {
        globalenv.verbose = parseFlag(&valueok, value);
    } else if (!cmp(param, "globalnotifications")) {
        globalenv.globalnotifications = parseFlag(&valueok, value);
    } else if (!cmp(param, "no_update_notifications")) {
        globalenv.reqflags.no_update_notifications = parseFlag(&valueok, value);
    } else if (!cmp(param, "no_add_notifications")) {
        globalenv.reqflags.no_add_notifications = parseFlag(&valueok, value);
    } else if (!cmp(param, "no_delete_notifications")) {
        globalenv.reqflags.no_delete_notifications = parseFlag(&valueok, value);
    } else if (!cmp(param, "no_custom_notifications")) {
        globalenv.reqflags.no_custom_notifications = parseFlag(&valueok, value);
    } else if (!cmp(param, "notifications_only")) {
        globalenv.reqflags.notifications_only = parseFlag(&valueok, value);
    } else if (!cmp(param, "no_object_updates")) {
        globalenv.reqflags.no_object_updates = parseFlag(&valueok, value);
    } else if (!cmp(param, "anonymous")) {
        globalenv.reqflags.untrusted = parseFlag(&valueok, value);
    } else if (!cmp(param, "timeout")) {
        globalenv.request_timeout = strtol(value, NULL, 0);
        globalenv.reqflags.wait = 0;
    } else if (!cmp(param, "wait")) {
        globalenv.request_timeout = strtol(value, NULL, 0);
        globalenv.reqflags.wait = 1;
    } else if (!cmp(param, "shellify") || !cmp(param, "sh")) {
        globalenv.reqflags.shellify = parseFlag(&valueok, value);
    } else if (!cmp(param, "uglify")) {
        globalenv.reqflags.uglify = parseFlag(&valueok, value);
    } else if (!cmp(param, "daemon")) {
        if (globalenv.pcb)
            goto toolate;
        globalenv.daemon = parseFlag(&valueok, value);
    } else if (!cmp(param, "pidfile")) {
        if (globalenv.pcb)
            goto toolate;
        slprintf(globalenv.pidfile, sizeof(globalenv.pidfile), "%s", value);
#if !defined(CONFIG_PCB_CLI_STATIC) && defined(CONFIG_PCB_HTTP_SERIALIZER)
    } else if (!cmp(param, "rpc_ws_output")) {
        globalenv.rpc_ws_output = parseFlag(&valueok, value);
        serialize_handlers_t *handlers = serialization_getHandlers(SERIALIZE_FORMAT(serialize_format_http, 0, 0));
        if (globalenv.rpc_ws_output && !handlers) {
            serialization_loadPlugins(NULL,"libpcb_serialize_http.so");
        }
    } else if (!cmp(param, "ws_version")) {
        globalenv.ws_version = strtol(value, NULL, 0);
#endif
    } else if (!cmp(param, "template_info")) {
        globalenv.reqflags.template_info = parseFlag(&valueok, value);
    } else if (!cmp(param, "precise_time_stamps")) {
        globalenv.reqflags.precise_time_stamps = parseFlag(&valueok, value);
    } else {
        logerr("Environmental setting %s does not exist", param);
        return false;
    }
    if (!valueok) {
        logerr("\"%s\" is an invalid value for environmental setting %s", value, param);
        return false;
    }
    return true;
toolate:
    logerr("Environmental setting %s can not be set at this time", param);
    return false;
}

bool isEnv(const char *param, bool ignorecase) {
    int (*cmp)(const char *, const char *) = ignorecase?strcasecmp:strcmp;
    return !cmp(param, "uri")
       ||  !cmp(param, "name")
       ||  !cmp(param, "user")
       ||  !cmp(param, "indexpath")
       ||  !cmp(param, "alias")
       ||  !cmp(param, "slashpath")
       ||  !cmp(param, "depth")
       ||  !cmp(param, "persistent")
       ||  !cmp(param, "non_persistent")
       ||  !cmp(param, "zip")
       ||  !cmp(param, "define")
       ||  !cmp(param, "path")
       ||  !cmp(param, "less")
       ||  !cmp(param, "quiet")
       ||  !cmp(param, "json")
       ||  !cmp(param, "validateonly")
       ||  !cmp(param, "exitonerr")
       ||  !cmp(param, "verbose")
       ||  !cmp(param, "globalnotifications")
       ||  !cmp(param, "no_update_notifications")
       ||  !cmp(param, "no_add_notifications")
       ||  !cmp(param, "no_delete_notifications")
       ||  !cmp(param, "no_custom_notifications")
       ||  !cmp(param, "notifications_only")
       ||  !cmp(param, "no_object_updates")
       ||  !cmp(param, "daemon")
       ||  !cmp(param, "pidfile")
       ||  !cmp(param, "timeout")
       ||  !cmp(param, "wait")
       ||  !cmp(param, "shellify")
       ||  !cmp(param, "uglify")
       ||  !cmp(param, "fireandforget")
       ||  !cmp(param, "template_info")
       ||  !cmp(param, "precise_time_stamps")
#if !defined(CONFIG_PCB_CLI_STATIC) && defined(CONFIG_PCB_HTTP_SERIALIZER)
       ||  !cmp(param, "rpc_ws_output")
       ||  !cmp(param, "ws_version")
#endif
       ;
}


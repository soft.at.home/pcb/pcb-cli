/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include <debug/sahtrace.h>

#include "util.h"
#include "env.h"

typedef enum _tabtype {
    tabtype_single,
    tabtype_double,
} tabtype_t;

typedef struct _tabctx {
    tabtype_t type;
    char buf[256];
    char *path;
    char *prefix;
    int prefixlen;
    int completelen;
    reqflags_t flags;
    request_t *req;
    void (*complete)(const char *completion);
} tabctx_t;

static tabctx_t ctx = {tabtype_single, "", ctx.buf, ctx.buf, 0, 0, reqflags_initializer, NULL, NULL};

static void processTabSingle(const char *result) {
    if (ctx.completelen == -1) {
        ctx.completelen = slprintf(ctx.prefix, sizeof(ctx.buf) - (ctx.prefix - ctx.buf), "%s", result);
    } else {
        int i, n = strlen(result);
        for(i=ctx.prefixlen; i<ctx.completelen && i<n && ctx.prefix[i] == result[i]; i++);
        ctx.completelen = i;
        ctx.prefix[ctx.completelen] = '\0';
    }
}

static bool replyHandler(request_t *req, pcb_t *pcb, peer_info_t *from, void *userdata) {
    (void)pcb; (void)from; (void)userdata;
    reply_t *reply = request_reply(req);
    if (!reply)
        return true;

    beginOutput();

    reply_item_t *item;
    parameter_t *parameter;
    function_t *function;
    object_t *object;
    string_t s;
    string_initialize(&s, 256);
    for(item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
        if (reply_item_type(item) != reply_type_object)
            continue;
        object = reply_item_object(item);
        object_path(object, &s, (ctx.flags.indexpath?0:path_attr_key_notation) | (ctx.flags.slashpath?path_attr_slash_seperator:0));
        SAH_TRACE_INFO("Returned object: [%s]",string_buffer(&s));
        if (string_compareChar(&s,request_path(req),0) == 0) {
            // object path is the same as the request path,
            // so only do parameters and functions
            object_for_each_parameter(parameter,object) {
                if (!strncmp(parameter_name(parameter), ctx.prefix, ctx.prefixlen)) {
                    if (ctx.type == tabtype_double) {
                        output("%s", parameter_name(parameter));
                    } else {
                        processTabSingle(parameter_name(parameter));
                    }
                }
            }
            object_for_each_function(function,object) {
                if (!strncmp(function_name(function), ctx.prefix, ctx.prefixlen)) {
                    if (ctx.type == tabtype_double) {
                        function_details(function, &s);
                        output("%s%s", function_name(function), string_buffer(&s));
                    } else {
                        string_clear(&s);
                        string_appendFormat(&s, "%s%s", function_name(function), ctx.flags.define?"":"(");
                        processTabSingle(string_buffer(&s));
                    }
                }
            }
        } else {
            // it is a child or instance of the requested path
            size_t plen = strlen(request_path(req)) + 1;
            if (string_length(&s) > plen && strchr(string_buffer(&s) + plen, ctx.flags.slashpath?'/':'.'))
                continue; // deeper child -> ignore
            if (object_name(object,ctx.flags.indexpath?0:path_attr_key_notation) && !strncmp(object_name(object, ctx.flags.indexpath?0:path_attr_key_notation), ctx.prefix, ctx.prefixlen)) {
                if (ctx.type == tabtype_double) {
                    output("%s%c", object_name(object, ctx.flags.indexpath?0:path_attr_key_notation), ctx.flags.slashpath?'/':'.');
                } else {
                    string_clear(&s);
                    string_appendFormat(&s, "%s%c", object_name(object, ctx.flags.indexpath?0:path_attr_key_notation), ctx.flags.slashpath?'/':'.');
                    processTabSingle(string_buffer(&s));
                }
            }
        }
    }
    string_cleanup(&s);
    endOutput();
    return true;
}

static bool doneHandler(request_t *req, pcb_t *pcb, peer_info_t *from, void *userdata) {
    (void)req; (void)pcb; (void)from; (void)userdata;
    if (ctx.type == tabtype_single && ctx.completelen > ctx.prefixlen) {
        *(ctx.prefix + ctx.completelen) = '\0';
        ctx.complete(ctx.prefix + ctx.prefixlen);
    }
    request_destroy(req);
    return true;
}

static void destroyHandler(request_t *req, void *userdata) {
    (void)req; (void)userdata;
    ctx.req = NULL;
}

void tab_cancel() {
    if (ctx.req)
        request_destroy(ctx.req);
}

static void tab_invoke(const char * stringsofar) {
    if (ctx.req) {
        request_destroy(ctx.req);
        ctx.req = NULL;
    }
    ctx.completelen = -1;
    ctx.flags = globalenv.reqflags;
    if (!parseCommandFlags(&ctx.flags, stringsofar, &stringsofar))
        goto cancel;
    if (!strncmp(stringsofar, "cd", 2) && (!stringsofar[2] || strchr(" \t\n", stringsofar[2]))) { // allow path completion on "cd"-commands
        stringsofar += 2;
        while (*stringsofar == ' ' || *stringsofar == '\t')
            stringsofar++;
    }
    int neededchars = snprintf(ctx.buf, sizeof(ctx.buf), "%s%s%s", *stringsofar == (ctx.flags.slashpath?'/':'.') ? "" : globalenv.path,
            *globalenv.path ? (ctx.flags.slashpath?"/":".") : "", stringsofar);
    if ((unsigned)neededchars >= sizeof(ctx.buf)) {
        logerr("path too long");
        goto cancel;
    }
    ctx.path = ctx.buf;
    ctx.prefix = NULL;
    while (*ctx.path) {
        if (*ctx.path == '(' && ctx.type == tabtype_double) {
            *ctx.path = '\0'; // print function definition for information
        } else if (strchr("(=+{?", *ctx.path)) {
            goto cancel;
        } else if (*ctx.path == (ctx.flags.slashpath?'/':'.')) {
            ctx.prefix = ctx.path;
        }
        ctx.path++;
    }
    if (ctx.prefix) {
        ctx.path = ctx.buf;
        *ctx.prefix++ = '\0';
    } else {
        ctx.path = "";
        ctx.prefix = ctx.buf;
    }
    ctx.prefixlen = strlen(ctx.prefix);
    ctx.req = request_create_getObject(ctx.path, 1,
            (ctx.flags.indexpath?0:request_common_path_key_notation) |
            (ctx.flags.slashpath?request_common_path_slash_seperator:0) |
            (globalenv.reqflags.untrusted?request_received_over_tcp:0) |
            request_getObject_parameters |
            request_getObject_functions |
            request_getObject_instances |
            request_no_object_caching |
            (ctx.flags.define || ctx.flags.template_info ? request_getObject_template_info : 0));
    if (!ctx.req) {
        logerr("request_create_getObject(path=%s) failed", ctx.path);
        goto cancel;
    }
    SAH_TRACE_INFO("Sending request for path [%s]",ctx.path);
    request_setReplyHandler(ctx.req, replyHandler);
    request_setDestroyHandler(ctx.req, destroyHandler);
    request_setDoneHandler(ctx.req, doneHandler);
    if (!pcb_sendRequest(globalenv.pcb, globalenv.peer, ctx.req)) {
        logerr("pcb_sendRequest() failed");
        goto cancel;
    }
    return;
cancel:
    if (ctx.req)
        request_destroy(ctx.req);
}

void tab_single(const char * stringsofar, void (*complete)(const char *completion)) {
    tab_cancel();
    ctx.type = tabtype_single;
    snprintf(ctx.buf, sizeof(ctx.buf), "%s", stringsofar);
    ctx.complete = complete;
    tab_invoke(stringsofar);
}

void tab_double(const char * stringsofar) {
    tab_cancel();
    ctx.type = tabtype_double;
    snprintf(ctx.buf, sizeof(ctx.buf), "%s", stringsofar);
    tab_invoke(stringsofar);
}


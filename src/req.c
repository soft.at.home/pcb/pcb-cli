/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#define _GNU_SOURCE
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include <pcb/common.h>
#include <pcb/utils.h>
#include <pcb/core.h>
#include <pcb/pcb_client.h>

#include "util.h"
#include "env.h"
#include "filter.h"
#include "req.h"

typedef enum _req_type {
    req_type_get,
    req_type_open,
    req_type_set,
    req_type_claim,
    req_type_add,
    req_type_del,
    req_type_exec,
    req_type_close,
    req_type_filter,
} req_type_t;

typedef enum _req_argtype {
    req_argtype_param_required,
    req_argtype_param_optional,
    req_argtype_param_only,
} req_argtype_t;

typedef struct _req_arg {
    llist_iterator_t it;
    char *param;
    variant_t value;
} req_arg_t;

typedef struct _req {
    llist_iterator_t it;
    req_type_t type;
    char *path;
    reqflags_t flags;
    int depth;
    int index;
    char *key;
    char *function;
    char *remainder;
    filter_t *filter;
    llist_t args;
    request_t *request;
    char *buf;
    bool found;
    pcb_timer_t *timer;
    int updates;
} req_t;

llist_t reqs = {NULL, NULL};

static char *pendingreq = NULL;
static char missing = '\0';

static bool req_submit(req_t *req);
static parse_code_t req_parse_internal(req_t *req);

static void req_arg_destroy(req_arg_t *arg) {
    llist_iterator_take(&arg->it);
    variant_cleanup(&arg->value);
    free(arg);
}

static parse_code_t req_arg_create(req_t *req, char *param, char *value, char **endptr) {
    req_arg_t *arg = calloc(1, sizeof(req_arg_t));
    if (!arg) {
        logerr("out of memory exception @%s:%d", __FILE__, __LINE__);
        return parse_error;
    }
    arg->param = param;
    variant_initialize(&arg->value, variant_type_unknown);
    if (value) {
        parse_code_t ret = variant_parse(&arg->value, value, endptr);
        if (ret == parse_error)
            logerr("Invalid value for param %s", param?param:"(anonymous)");
        if (ret != parse_success) {
            req_arg_destroy(arg);
            return ret;
        }
    }
    llist_append(&req->args, &arg->it);
    return parse_success;
}

static req_t *req_create() {
    req_t *req = calloc(1, sizeof(req_t));
    if (!req) {
        logerr("out of memory exception @%s:%d", __FILE__, __LINE__);
        return NULL;
    }
    llist_append(&reqs, &req->it);
    globalenv.workload++;
    return req;
}

static void req_destroy(req_t *req) {
    if (!req)
        return;
    if (req->request) {
        request_setData(req->request, NULL);
        request_destroy(req->request);
    }
    while (llist_first(&req->args))
        req_arg_destroy((req_arg_t *)llist_first(&req->args));
    llist_iterator_take(&req->it);
    filter_destroy(req->filter);
    pcb_timer_destroy(req->timer);
    free(req->key);
    free(req->buf);
    free(req);
    globalenv.workload--;
}


static bool req_checkArg(req_t *req, const char *param, bool explicit) {
    if (llist_isEmpty(&req->args) || request_type(req->request) == request_type_create_instance)
        return !explicit;
    req_arg_t *arg;
    for (arg = (req_arg_t *)llist_first(&req->args); arg; arg = (req_arg_t *)llist_iterator_next(&arg->it)) {
        if (arg->param && !strcmp(arg->param, param))
            return true;
    }
    return false;
}

static void req_respin(req_t *req) {
    request_t *request = req->request;
    req->request = NULL;
    request_destroy(request);
    if (!req_submit(req))
        req_destroy(req);
}

static bool handleNotFound(req_t *req,reply_item_t *item) {
    char *param;
    if ((req->type == req_type_get || req->type == req_type_open) && llist_isEmpty(&req->args)) {
        // get object didn't work, try "path.param?" notation
        param = strrchr(req->path, req->flags.slashpath?'/':'.');
        if (!param)
            goto error_anyway;
        *param++ = '\0';
        req->depth = 0;
        if (req_arg_create(req, param, NULL, NULL) == parse_error)
            return false;
        req_respin(req);
        return true;
    } else if (req->type == req_type_claim) {
        req->type = req_type_add;
        param = strrchr(req->path, req->flags.slashpath?'/':'.');
        if (!param)
            goto error_anyway;
        *param++ = '\0';
        if (req->flags.indexpath) {
            req->index = strtoul(param, NULL, 0);
        } else {
            req->key = strdup(param);
        }
        req_respin(req);
        return true;
    } else {
error_anyway:
        if (item) {
            logerr("0x%8.8X %s: %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
        } else {
            llist_iterator_t *it = llist_first(&req->args);
            if (it) {
                req_arg_t *arg = llist_item_data(it,req_arg_t,it);
                logerr("0x%8.8X %s: %s.%s", pcb_error_not_found, error_string(pcb_error_not_found), req->path, arg->param);
            } else {
                logerr("0x%8.8X %s: %s", pcb_error_not_found, error_string(pcb_error_not_found), req->path);
            }
        }
        if (globalenv.exitonerr)
            globalenv.exit = true;
    }
    return false;
}

static bool replyHandler(request_t *request, pcb_t *pcb, peer_info_t *from, void *userdata) {
    (void)pcb; (void)from;
    req_t *req = (req_t *)userdata;
    if (!req || req->request != request)
        return true;
    reply_t *reply = request_reply(request);
    if (!reply)
        return true;

    beginOutput();

    reply_item_t *item;
    notification_t *notification;
    object_t *object;
    const variant_t *retval;
    argument_value_list_t *args;
    argument_value_t *argval;
    string_t s1, s2;
    parameter_t *parameter;
    function_t *function;
    notification_parameter_t *nfparam;
    int n1;
    req_t *respin;
    bool error = false;

    if (!string_initialize(&s1, 256) || !string_initialize(&s2, 256)) {
        logerr("out of memory exception @%s:%d", __FILE__, __LINE__);
        goto leave;
    }
    for(item = reply_firstItem(reply); item; item = reply_nextItem(item)) {
        switch(reply_item_type(item)) {
            case reply_type_object:
                object = reply_item_object(item);
                if (req->type == req_type_claim) {
                    req->type = req_type_set;
                    req_respin(req);
                    break;
                }
                if (req->type != req_type_get && req->type != req_type_open && req->type != req_type_filter
                        && !globalenv.interactive && !globalenv.verbose)
                    break;
                if (req->type == req_type_open && req->updates) {
                    req->updates--;
                    break;
                }
                object_path(object, &s1, (req->flags.indexpath?0:path_attr_key_notation)
                        | (req->flags.slashpath?path_attr_slash_seperator:0));
                if (req->type == req_type_filter) {
                    if (!string_compareChar(&s1, req->path, 0))
                        break;
                    if (!filter_test(req->filter, object, &req->flags))
                        break;
                    respin = req_create();
                    if (!respin)
                        break;
                    respin->flags = req->flags;
                    if (asprintf(&respin->buf, "%s%c%s%s"
                            , req->path
                            , req->flags.slashpath?'/':'.'
                            , object_name(object, req->flags.indexpath?0:path_attr_key_notation)
                            , req->remainder) == -1) {
                        logerr("out of memory exception @%s:%d", __FILE__, __LINE__);
                        req_destroy(respin);
                        break;
                    }
                    if (req_parse_internal(respin) != parse_success)
                        req_destroy(respin);
                } else if (req->flags.define) {
                    if (llist_isEmpty(&req->args)) {
                        object_details(object, &s2);
                        output("%s%s", string_buffer(&s1), string_buffer(&s2));
                    }
                    object_for_each_parameter(parameter,object) {
                        if (!req_checkArg(req, parameter_name(parameter), false))
                            continue;
                        parameter_details(parameter, &s2);
                        output("%s%c%s%s", string_buffer(&s1), req->flags.slashpath?'/':'.', parameter_name(parameter), string_buffer(&s2));
                    }
                    object_for_each_function(function, object) {
                        if (!req_checkArg(req, function_name(function), false))
                            continue;
                        function_details(function, &s2);
                        output("%s%c%s%s", string_buffer(&s1), req->flags.slashpath?'/':'.', function_name(function), string_buffer(&s2));
                    }
                    req->found = true;
                } else {
                    if (llist_isEmpty(&req->args) && !req->flags.less && !req->flags.quiet) {
                        output("%s", string_buffer(&s1));
                        req->found = true;
                    }
                    if (object_isInstance(object) && req_checkArg(req, "key", true)) {
                         output("%s%ckey=%s", string_buffer(&s1), req->flags.slashpath?'/':'.', object_name(object, path_attr_key_notation));
                         req->found = true;
                    }
                    if (object_isInstance(object) && req_checkArg(req, "index", true)) {
                         output("%s%cindex=%s", string_buffer(&s1), req->flags.slashpath?'/':'.', object_name(object, 0));
                        req->found = true;
                    }
                    object_for_each_parameter(parameter,object) {
                        if (!req_checkArg(req, parameter_name(parameter), false))
                            continue;
                        variant_cliprint(&s2, parameter_getValue(parameter), &req->flags);
                        if (req->flags.less)
                            output("%s", string_buffer(&s2));
                        else if (!req->flags.quiet)
                            output("%s%c%s=%s", string_buffer(&s1), req->flags.slashpath?'/':'.',
                                    parameter_name(parameter), string_buffer(&s2));
                        req->found = true;
                    }
                }
                break;
            case reply_type_notification: {
                pcb_datetime_t dt;
                pcb_datetime_now(&dt);
                string_t currentTime;
                string_initialize(&currentTime, 0);
                string_fromTimeExtended(&currentTime, &dt, globalenv.reqflags.precise_time_stamps ? ISO8601_DATE_TIME_FMT_US : ISO8601_DATE_TIME_FMT);
                notification = reply_item_notification(item);
                switch (notification_type(notification)) {
                    case notify_value_changed:
                        nfparam = notification_getParameter(notification, "parameter");
                        variant_toString(&s1, notification_parameter_variant(nfparam));
                        nfparam = notification_getParameter(notification, "newvalue");
                        variant_cliprint(&s2, notification_parameter_variant(nfparam), &req->flags);
                        if (req->flags.less)
                            output("%s", string_buffer(&s2));
                        else if (!req->flags.quiet) {
                            output("[%s] %s%c%s=%s", string_buffer(&currentTime), notification_objectPath(notification), req->flags.slashpath ? '/' : '.', string_buffer(&s1), string_buffer(&s2));
                        }
                        req->updates++;
                        break;
                    case notify_object_deleted:
                        if (!req->flags.less && !req->flags.quiet)
                            output("[%s] %s-", string_buffer(&currentTime), notification_objectPath(notification));
                        break;
                    case notify_object_added:
                        if (!req->flags.less && !req->flags.quiet)
                            output("[%s] %s+", string_buffer(&currentTime), notification_objectPath(notification));
                        break;
                    default:
                        if (req->flags.no_custom_notifications)
                            break;
                        if (req->flags.less) {
                            output("[%s|%d]", notification_name(notification)?notification_name(notification):"custom", notification_type(notification));
                        } else if (!req->flags.quiet) {
                            notification_details(notification, &s1, &req->flags);
                            output("[%s] %s[%s|%d]%s", string_buffer(&currentTime), notification_objectPath(notification), notification_name(notification)?:"custom",
                                   notification_type(notification), string_buffer(&s1));
                        }
                        break;
                }
                string_cleanup(&currentTime);
                break;
            }
            case reply_type_function_return:
                retval = reply_item_returnValue(item);
                args = reply_item_returnArguments(item);
                    string_clear(&s1);
                    n1 = 0;
                    for (argval=argument_valueFirstArgument(args); argval; argval = argument_valueNextArgument(argval)) {
                        variant_cliprint(&s2, argument_value(argval), &req->flags);
                        string_appendFormat(&s1, "%s%s=%s", n1++?", ":"", argument_valueName(argval), string_buffer(&s2));
                    }
                    if (variant_type(retval) == variant_type_unknown) {
                        if (!req->flags.less && !req->flags.quiet)
                            output("%s%c%s(%s) %s", req->path, req->flags.slashpath?'/':'.', req->function, string_buffer(&s1), error?"failed":"done");
                    } else {
                        variant_cliprint(&s2, retval, &req->flags);
                        if (req->flags.less) {
                            output("%s", string_buffer(&s2));
                        } else if (!req->flags.quiet) {
                            output("%s%c%s(%s) returns %s", req->path, req->flags.slashpath?'/':'.', req->function, string_buffer(&s1), string_buffer(&s2));
                        }
                    }
                break;
            case reply_type_error:
                if (reply_item_error(item) == pcb_error_not_found
                        && (req->type == req_type_get || req->type == req_type_open || req->type == req_type_claim)) {
                    handleNotFound(req,item);
                } else {
                    error = true;
                    logerr("0x%8.8X %s: %s", reply_item_error(item), reply_item_errorDescription(item), reply_item_errorInfo(item));
                    if (globalenv.exitonerr)
                        globalenv.exit = true;
                }
                break;
            default:
                break;
        }
    }
leave:
    string_cleanup(&s1);
    string_cleanup(&s2);
    endOutput();
    return true;
}

static void timeoutHandler(pcb_timer_t *timer, void *userdata) {
    req_t *req = (req_t *)userdata;
    if (req->timer != timer)
        return;
    if (globalenv.exitonerr)
        globalenv.exit = true;
    logerr("request timed out");
    req_destroy(req);
}

static bool doneHandler(request_t *request, pcb_t *pcb, peer_info_t *from, void *userdata) {
    (void)pcb; (void)from;
    req_t *req = (req_t *)userdata;
    if (!req || req->request != request)
        return true;
    pcb_timer_stop(req->timer);
    if (!req->found && (req->type == req_type_get || req->type == req_type_open || req->type == req_type_claim)
            && handleNotFound(req,NULL))
        return true;
    req_destroy(req);
    return true;
}

static bool req_submit(req_t *req) {
    int n = strlen(req->path); // never pass a path to pcb that ends with path separator
    if (n > 0 && req->path[n-1] == (req->flags.slashpath?'/':'.'))
        req->path[n-1] = '\0';

    if (req->type == req_type_close) { // close request is a special case.
        bool destroyed = false;
        req_t *r, *rnext;
        for (r = (req_t *)llist_first(&reqs); r; r = rnext) {
            rnext = (req_t *)llist_iterator_next(&r->it);
            if (r->type == req_type_open && !strcmp(r->path, req->path)) {
                destroyed = true;
                req_destroy(r);
            }
        }
        if (!destroyed)
            logerr("No matching subscription found");
        req_destroy(req);
        return true;
    }

    uint32_t attr = request_no_object_caching
                  | (req->flags.indexpath? 0: request_common_path_key_notation)
                  | (req->flags.alias? request_common_path_alias : 0)
                  | (req->flags.slashpath? request_common_path_slash_seperator : 0)
                  | (req->flags.wait? request_wait : 0);
    req_arg_t *arg = NULL;

    // create request depending on req type.
    switch(req->type) {
        case req_type_open:
        case req_type_get:
            if (!req->flags.zip)
                attr |= request_getObject_parameters | request_getObject_children | request_getObject_functions | request_getObject_instances;
            if (req->flags.untrusted)
                attr |= request_received_over_tcp;
            if (req->flags.define)
                attr = (attr | request_getObject_template_info | request_getObject_validators | request_getObject_description) & ~request_getObject_instances;
            else
                attr &= ~request_getObject_functions;
            if (req->flags.template_info)
                attr |= request_getObject_template_info;
            if (req->type == req_type_open) {
                if (!req->flags.no_update_notifications)
                    attr |= request_notify_values_changed;
                if (!req->flags.no_add_notifications)
                    attr |= request_notify_object_added;
                if (!req->flags.no_delete_notifications)
                    attr |= request_notify_object_deleted;
                if (!req->flags.no_custom_notifications)
                    attr |= request_notify_custom;
                if (req->flags.notifications_only)
                    attr |= request_notify_only;
                if (req->flags.no_object_updates)
                    attr |= request_notify_no_updates;
            }
            req->request = request_create_getObject(req->path, req->depth, attr);
            break;
        case req_type_filter:
            attr |= request_getObject_parameters;
            if (req->flags.untrusted)
                attr |= request_received_over_tcp;
            req->request = request_create_getObject(req->path, 1, attr);
            break;
        case req_type_add:
            if (req->flags.persistent)
                attr |= request_createObject_persistent;
            if (req->flags.non_persistent)
                attr |= request_createObject_not_persistent;
            if (req->flags.untrusted)
                attr |= request_received_over_tcp;
            req->request = request_create_addInstance(req->path, req->index, req->key, attr);
            break;
        case req_type_del:
            if (req->flags.untrusted)
                attr |= request_received_over_tcp;
            req->request = request_create_deleteInstance(req->path, attr);
            break;
        case req_type_set:
            if (req->flags.untrusted)
                attr |= request_received_over_tcp;
            if (req->flags.validateonly)
                attr |= request_setObject_validate_only;
            req->request = request_create_setObject(req->path, attr);
            break;
        case req_type_claim:
            if (req->flags.untrusted)
                attr |= request_received_over_tcp;
            req->request = request_create_getObject(req->path, 0, attr);
            break;
        case req_type_exec:
            for (arg = (req_arg_t *)llist_first(&req->args); arg; arg = (req_arg_t *)llist_iterator_next(&arg->it))
                if (!arg->param)
                    break;
            if (!arg)
                attr |= request_function_args_by_name;
            if (req->flags.untrusted)
                attr |= request_received_over_tcp;
            req->request = request_create_executeFunction(req->path, req->function, attr);
            break;
        default:
            break;
    }
    if (!req->request) {
        logerr("request creation failed");
        return false;
    }
    request_setData(req->request, req);
    request_setReplyHandler(req->request, replyHandler);
    request_setDoneHandler(req->request, doneHandler);

    if (req->type != req_type_claim) {
        // add arguments if there are any
        argument_value_t *argval = NULL;
        for (arg = (req_arg_t *)llist_first(&req->args); arg; arg = (req_arg_t *)llist_iterator_next(&arg->it)) {
            argval = argument_valueCreate(arg->param?arg->param:"_");
            if (!argval)
                break;
            if (variant_type(&arg->value) != variant_type_unknown)
                variant_copy(argument_value(argval), &arg->value);
            if (!request_addArgument(req->request, argval))
                break;
            argval = NULL;
        }
        if (arg) {
            logerr("add request argument %s failed", arg->param?arg->param:"(anonymous)");
            if (argval)
                argument_valueDestroy(argval);
            return false;
        }
    }

    if (req->type == req_type_set && llist_isEmpty(&req->args)) {
        req_destroy(req);
        return true;
    }

    if (globalenv.request_timeout) {
        req->timer = pcb_timer_create();
        pcb_timer_setUserData(req->timer,req);
        pcb_timer_setHandler(req->timer,timeoutHandler);
        pcb_timer_start(req->timer,globalenv.request_timeout * 1000);
    }

    if (!pcb_sendRequest(globalenv.pcb, globalenv.peer, req->request)) {
        logerr("pcb_sendRequest failed");
        return false;
    }

    if (globalenv.fireandforget) {
        req_destroy(req);
    }

    return true;
}

static parse_code_t req_parseArgs(req_t *req, char *str, char **endptr, char argstop, req_argtype_t type) {
    char *param = NULL, *value;
    bool finished = false;
    parse_code_t tmpret;
    do {
        while (*str && (*str <= ' ' || *str == ',')) // skip whitepace and commas
            str++;
        if (!*str) {
            missing = argstop;
            return parse_we_want_more;
        }
        param = str;
        while (!strchr("=:,\"[{", *str) && *str != argstop && *str > ' ') // skip param
            str++;
        if (*str == argstop)
            finished = true;
        if (param == str && finished)
            break;
        while (*str && *str <= ' ') // skip whitepace again
            *str++ = '\0';
        // now str points to start of value or start of next parameter
        value = (*str == '=' || *str == ':') ? str+1 : strchr("\"[{", *str) ? str : NULL;
        if (param == value || !*param)
            param = NULL;
        if (!value && type == req_argtype_param_optional) {
            value = param;
            param = NULL;
        }
        if ((!value || !param) && type == req_argtype_param_required) {
            logerr("Only param=value pairs are allowed in the parameter list");
            return parse_error;
        } else if (value && type == req_argtype_param_only) {
            logerr("Only parameter names are allowed in the parameter list");
            return parse_error;
        }
        if (param && (strchr("=:,\"[{", *str) || *str == argstop))
            *str++ = '\0';
        tmpret = req_arg_create(req, param, value, str<=value?&str:NULL);
        if (tmpret != parse_success)
            return tmpret;
    } while (!finished);
    if (endptr)
        *endptr = str;
    return parse_success;
}

static parse_code_t req_parse_internal(req_t *req) {
    parse_code_t tmpret, ret = parse_error;
    char *param, *value;
    char *bufptr = req->path = req->buf;
    req_arg_t *arg, *argnext;
    bool ampersand = false;

    if (*bufptr && *(bufptr + strlen(bufptr) - 1) == '&') {
        ampersand = true;
        req->flags.wait = 1;
        *(bufptr + strlen(bufptr) - 1) = 0;
    }

    do {
        if (*bufptr == '\\') {
            memmove(bufptr, bufptr + 1, strlen(bufptr) - 1);
            *(bufptr + strlen(bufptr) - 1) = 0;
            bufptr++;
            continue;
        }
        if (*bufptr == '[') { // filter
            req->type = req_type_filter;
            req->filter = filter_parse(bufptr, &req->remainder);
            *bufptr = '\0';
            if (!req->filter)
                return ret;
        } else if (*bufptr == '-' && *(bufptr+1) == '\0') { // delete request (the minus has to be the last character!)
            *bufptr = '\0';
            req->type = req_type_del;
        } else if (*bufptr == '?') { // get request
            *bufptr++ = '\0';
            value = bufptr;
            req->type = req_type_get;
            req->depth = (uint32_t)strtol(bufptr, &bufptr, 0);
            if (bufptr == value)
                req->depth = globalenv.depth;
            if (*bufptr == '{') {
                if ((ret = req_parseArgs(req, bufptr+1, &bufptr, '}', req_argtype_param_only)) != parse_success)
                    return ret;
            }
            if (ampersand) {
                req->type = req_type_open;
            } else if (*bufptr == '$') {
                req->type = req_type_close;
            }
        } else if (*bufptr == '(') { // execute function
            *bufptr++ = '\0';
            req->type = req_type_exec;
            req->function = strrchr(req->path, req->flags.slashpath?'/':'.');
            if (req->function) {
                *req->function++ = '\0';
            } else {
                req->function = req->path;
                req->path = "";
            }
            if ((ret = req_parseArgs(req, bufptr, NULL, ')', req_argtype_param_optional)) != parse_success)
                return ret;
        } else if (*bufptr == '=') { // parameter setting
            *bufptr++ = '\0';
            req->type = req_type_set;
            param = strrchr(req->path, req->flags.slashpath?'/':'.');
            if (param) {
                *param++ = '\0';
            } else {
                param = req->path;
                req->path = "";
            }
            tmpret = req_arg_create(req, param, bufptr, NULL);
            if (tmpret != parse_success)
                return tmpret;
        } else if (*bufptr == '+' || *bufptr == '{' || *bufptr == '\0') { // add/claim instance
            req->type = *bufptr == '+' ? req_type_add : req_type_claim;
            if (*bufptr == '+')
                *bufptr++ = '\0';
            if (*bufptr == '{') {
                if ((ret = req_parseArgs(req, bufptr+1, NULL, '}', req_argtype_param_required)) != parse_success)
                    return ret;
            }
            *bufptr = '\0';
            for (arg = (req_arg_t *)llist_first(&req->args); arg; arg = argnext) {
                argnext = (req_arg_t *)llist_iterator_next(&arg->it);
                if (!strcmp(arg->param, "key") && !req->key) {
                    req->key = variant_char(&arg->value);
                    req_arg_destroy(arg);
                } else if (!strcmp(arg->param, "index") && !req->index) {
                    req->index = variant_int32(&arg->value);
                    req_arg_destroy(arg);
                }
            }
        } else {
            bufptr++;
            continue;
        }
        break;
    } while (1);
    return req_submit(req) ? parse_success : parse_error;
}

parse_code_t req_parse(char *buf, int buflen) {
    (void)buflen;
    char *bufptr;
    req_t *req = NULL;
    parse_code_t ret = parse_error;

    // remove trailing spaces
    bufptr = buf + strlen(buf);
    if (bufptr > buf) do {
        bufptr--;
    } while (*bufptr == ' ' || *bufptr == '\t');
    *++bufptr = '\0';

    // remove leading spaces
    bufptr = buf;
    while (*bufptr == ' ' || *bufptr == '\t')
        bufptr++;

    // handle empty lines
    if (!*bufptr) {
        if (pendingreq) {
            logerr("%c missing", missing);
            free(pendingreq);
            pendingreq = NULL;
            return parse_error;
        } else {
            return parse_success;
        }
    }

    // handle comments
    if (*bufptr == '#')
        return pendingreq ? parse_we_want_more : parse_success;

    // handle environmental settings
    char *param = bufptr;
    char *value;
    if ((value = strchr(bufptr, '='))) {
        *value++ = '\0';
        if (isEnv(param, 0))
            return setEnv(param, value, 0)?parse_success:parse_error;
        *--value = '=';
    }

    // handle real request ...
    req = req_create();
    if (!req)
        goto error;

    // parse flags
    req->flags = globalenv.reqflags;
    if (!parseCommandFlags(&req->flags, bufptr, (const char **)&bufptr)) {
        logerr("Invalid request flag detected. Hint: request flags have to be space-terminated!");
        goto error;
    }

    // precede command by environmental path
    if (asprintf(&req->buf, "%s%s%s%s%s"
            , *bufptr == '.' ? "" : globalenv.path
            , (*bufptr && !strchr(".?-+{=", *bufptr) && *globalenv.path) ? (globalenv.reqflags.slashpath?"/":".") : ""
            , pendingreq?:"", pendingreq?" ":"", bufptr) == -1) {
        logerr("out of memory exception @%s:%d", __FILE__, __LINE__);
        goto error;
    }
    free(pendingreq);
    pendingreq = strdup(req->buf);
    ret = req_parse_internal(req);
    if (ret != parse_we_want_more) {
        free(pendingreq);
        pendingreq = NULL;
    }
error:
    if (ret != parse_success)
        req_destroy(req);
    return ret;
}

void req_close() {
    while (llist_first(&reqs))
        req_destroy((req_t *)llist_first(&reqs));
    free(pendingreq);
}

void req_list() {
    req_t *req;
    beginOutput();
    output("\tRequest id\tRequest type\tRequest path");
    for (req = (req_t *)llist_first(&reqs); req; req = (req_t *)llist_iterator_next(&req->it)) {
        output("\t%10d\t%12d\t%s",request_id(req->request),request_type(req->request),request_path(req->request));
    }
    endOutput();
}

void req_close_pending(uint32_t id) {
    req_t *req;
    beginOutput();
    for (req = (req_t *)llist_first(&reqs); req; req = (req_t *)llist_iterator_next(&req->it)) {
        if (request_id(req->request) == id) {
            break;
        }
    }
    if (req) {
        req_destroy(req);
        output("Closed request");
    } else {
        output("Request id %d not found",id);
    }
    endOutput();
}

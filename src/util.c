/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <string.h>

#include <pcb/utils.h>

#include "util.h"
#include "env.h"

int vslprintf(char * buf, int buflen, const char *fmt, va_list args) {
    if (buflen <= 0)
        return buflen-1;
    int n = vsnprintf(buf, buflen, fmt, args);
    return n > buflen-1 ? buflen-1 : n;
}

int slprintf(char * buf, int buflen, const char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    int n = vslprintf(buf, buflen, fmt, args);
    va_end(args);
    return n;
}

int slinsert(char *buf, int buflen, const char *str) {
    if (buflen <= 0)
        return buflen-1;
    int n = strlen(str);
    int m = strlen(buf);
    if (n + m + 1 > buflen) {
        m = buflen - n - 1;
        if (m < 0) {
            n = buflen - 1;
            m = 0;
        }
    }
    if (m > 0 && n > 0)
        memmove(buf + n, buf, m);
    if (n > 0)
        memcpy(buf, str, n);
    buf[n + m] = '\0';
    return n + m;
}

bool parseFlag(bool *ok, const char *str) {
    if (!strcasecmp(str, "yes") || !strcasecmp(str, "true")  || !strcasecmp(str, "y")) {
        if (ok) *ok = true;
        return true;
    } else if (!strcasecmp(str, "no") || !strcasecmp(str, "false")  || !strcasecmp(str, "n")) {
        if (ok) *ok = true;
        return false;
    } else {
        if (ok) *ok = false;
        return false;
    }
}

bool parseCommandFlags(reqflags_t *flags, const char *str, const char **endptr) {
    while (*str == '-') {
        str++;
        while (*str != ' ' && *str != '\t') {
            switch (*str) {
                case 'i': flags->indexpath = !flags->indexpath; break;
                case 's': flags->slashpath = !flags->slashpath; break;
                case 'd': flags->define = !flags->define; break;
                case 'z': flags->zip = !flags->zip; break;
                case 'p': flags->persistent = !flags->persistent; break;
                case 'P': flags->non_persistent = !flags->non_persistent; break;
                case 'l': flags->less = !flags->less; break;
                case 'q': flags->quiet = !flags->quiet; break;
                case 'j': flags->json = !flags->json; break;
                case 'V': flags->validateonly = !flags->validateonly; break;
                case 'U': flags->no_update_notifications = !flags->no_update_notifications; break;
                case 'A': flags->no_add_notifications = !flags->no_add_notifications; break;
                case 'D': flags->no_delete_notifications = !flags->no_delete_notifications; break;
                case 'C': flags->no_custom_notifications = !flags->no_custom_notifications; break;
                case 'N': flags->notifications_only = !flags->notifications_only; break;
                case 'O': flags->no_object_updates = !flags->no_object_updates; break;
                case 'a': flags->untrusted = !flags->untrusted; break;
                case 't': flags->template_info = !flags->template_info; break;
                case 'Z': flags->precise_time_stamps = !flags->precise_time_stamps; break;
                default:
                    return false;
            }
            str++;
        }
        while (*str == ' ' || *str == '\t')
            str++;
    }
    *endptr = str;
    return true;
}

static int hex2num(char c) {
    if (c >= '0' && c <= '9')
        return c - '0';
    else if (c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    else if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    else
        return 0;
}

static char *parseQuotedString(char *str, char **endptr) {
    char *s = str+1;
    char *d = str;
    if (*str != '"')
        return NULL;

    while (*s && *s != '"') {
        if (*s == '\\') {
            s++;
            if (!*s) {
                break;
            } else if (*s>='0' && *s<='7' && *(s+1) && *(s+2)) {
                *d++ = (hex2num(s[0])<<6) + (hex2num(s[1])<<3) + (hex2num(s[2]));
                s += 3;
            } else if (*s=='x' && *(s+1) && *(s+2)) {
                *d++ = (hex2num(s[1])<<4) + (hex2num(s[2]));
                s += 3;
            } else if (*s=='u' && *(s+1) && *(s+2) && *(s+3) && *(s+4)) {
                *d++ = (hex2num(s[1])<<12) + (hex2num(s[2])<<8) + (hex2num(s[3])<<4) + (hex2num(s[4]));
                s += 5;
            } else {
                switch (*s) {
                    case 'n': *d++ = '\n'; break;
                    case 'r': *d++ = '\r'; break;
                    case 'b': *d++ = '\b'; break;
                    case 't': *d++ = '\t'; break;
                    case 'f': *d++ = '\f'; break;
                    case 'a': *d++ = '\a'; break;
                    case 'v': *d++ = '\v'; break;
                    default: *d++ = *s; break;
                }
                s++;
            }
        } else {
            *d++ = *s++;
        }
    }
    if (!*s) {
        logerr("string not \"-terminated");
        return NULL;
    }
    *d = '\0';
    if (endptr)
        *endptr = s;
    return str;
}

bool variant_setQuotedString(variant_t *variant, char *str, char **endptr) {
    str = parseQuotedString(str, endptr);
    if (!str)
        return false;
    return variant_setChar(variant, str);
}

static parse_code_t variant_setJson(variant_t *variant, char *str, char **endptr, bool *empty);

static parse_code_t variant_setJsonMap(variant_t *variant, char *str, char **endptr) {
    parse_code_t tmpret, ret = parse_error;
    bool empty;
    char *key;
    char *keyend;
    variant_map_t map;
    variant_t item;
    variant_map_initialize(&map);
    variant_initialize(&item, variant_type_unknown);
    do {
        str++;
        while (*str && *str <= ' ')
            str++;
        key = str;
        while (*str > ' ' && !strchr("{}[]:,", *str))
            str++;
        keyend = str;
        while (*str && *str <= ' ')
            str++;
        if (key == keyend && *str == '}')
            break;
        if (!*str) {
            ret = parse_we_want_more;
            goto leave;
        }
        if (*str != ':')
            goto leave;
        *keyend = '\0';
        str++;
        tmpret = variant_setJson(&item, str, &str, &empty);
        if (tmpret != parse_success) {
            ret = tmpret;
            goto leave;
        }
        if (!*str) {
            ret = parse_we_want_more;
            goto leave;
        }
        if (empty)
            goto leave;
        if (*key == '"')
            key = parseQuotedString(key, NULL);
        if (!key)
            goto leave;
        if (!variant_map_add(&map, key, &item))
            goto leave;
        if (!*str) {
            ret = parse_we_want_more;
            goto leave;
        }
        if (*str != ',' && *str != '}')
            goto leave;
    } while (*str != '}');
    variant_setMapMove(variant, &map);
    ret = parse_success;
    *endptr = str;
leave:
    variant_cleanup(&item);
    variant_map_cleanup(&map);
    return ret;

}

static parse_code_t variant_setJsonList(variant_t *variant, char *str, char **endptr) {
    parse_code_t tmpret, ret = parse_error;
    bool empty;
    bool havesomething = false;
    variant_list_t list;
    variant_t item;
    variant_list_initialize(&list);
    variant_initialize(&item, variant_type_unknown);
    do {
        str++;
        tmpret = variant_setJson(&item, str, &str, &empty);
        if (tmpret != parse_success) {
            ret = tmpret;
            goto leave;
        }
        if (!*str) {
            ret = parse_we_want_more;
            goto leave;
        }
        if (empty) {
            if (!havesomething && *str == ']')
                break;
            else
                goto leave;
        }
        if (!variant_list_add(&list, &item))
            goto leave;
        if (*str != ',' && *str != ']')
            goto leave;
        havesomething = true;
    } while (*str != ']');
    variant_setListMove(variant, &list);
    ret = parse_success;
    *endptr = str;
leave:
    variant_cleanup(&item);
    variant_list_cleanup(&list);
    return ret;
}

static parse_code_t variant_setJson(variant_t *variant, char *str, char **endptr, bool *empty) {
    parse_code_t tmpret, ret = parse_error;
    char *str0, *str1;
    char strtmp;
    int64_t i; bool b; double d;
    while (*str && *str <= ' ') // ignore white space and special characters
        str++;
    if (empty)
        *empty = false;
    switch (*str) {
        case '{':
            tmpret = variant_setJsonMap(variant, str, &str);
            if (tmpret != parse_success) {
                ret = tmpret;
                goto leave;
            }
            str++;
            break;
        case '[':
            tmpret = variant_setJsonList(variant, str, &str);
            if (tmpret != parse_success) {
                ret = tmpret;
                goto leave;
            }
            str++;
            break;
        case '"':
            if (!variant_setQuotedString(variant, str, &str))
                goto leave;
            str++;
            break;
        case '}':
        case ']':
        case ')':
        case '\0':
        case ',':
            if (empty) {
                *empty = true;
            } else if (!variant_setChar(variant, "")) {
                goto leave;
            }
            break;
        default:
            str0 = str;
            while (*str > ' ' && !strchr(",}])", *str))
                str++;
            strtmp = *str;
            *str = '\0';
            if (!strcmp(str0, "null")) {
                variant_cleanup(variant);
                goto defaultok;
            }
            if (!variant_setBool(variant, parseFlag(&b, str0)))
                goto leave;
            if (b)
                goto defaultok;
            if (str0[1] == ';') {
                i = strtoll(str0 + 2, &str1, 0);
            } else {
                i = strtoll(str0, &str1, 0);
            }
            if (str1 == str) {
                if (str0[1] == ';') {
                    switch(str0[0]) {
                    case 'y': // 8-bit unsigned integer
                        if (!variant_setUInt8(variant, (uint8_t)i))
                            goto leave;
                        break;
                    case 'n': // 16-bit signed integer
                        if (!variant_setInt16(variant, (int16_t)i))
                            goto leave;
                        break;
                    case 'q': // 16-bit unsigned integer
                        if (!variant_setUInt16(variant, (uint16_t)i))
                            goto leave;
                        break;
                    case 'i': // 32-bit signed integer
                        if (!variant_setInt32(variant, (int32_t)i))
                            goto leave;
                        break;
                    case 'u': // 32-bit unsigned integer
                        if (!variant_setUInt32(variant, (uint32_t)i))
                            goto leave;
                        break;
                    case 'x': // 64-bit signed integer
                        if (!variant_setInt64(variant, i))
                            goto leave;
                        break;
                    case 't': // 64-bit unsigned integer
                        if (!variant_setUInt64(variant, (uint64_t)i))
                            goto leave;
                        break;
                    }
                } else {
                    if (!variant_setInt64(variant, i))
                        goto leave;
                }
                goto defaultok;
            }
            d = strtod(str0, &str1);
            if (str1 == str) {
                if (!variant_setDouble(variant, d))
                    goto leave;
                goto defaultok;
            }
            // Try to parse as date-time
            {
                pcb_datetime_t dt;
                const char *format = strchr(str0, '.') ? ISO8601_DATE_TIME_FMT_US : ISO8601_DATE_TIME_FMT;
                str1 = pcb_strptime(str0, format, &dt);
                if (str1 == str) {
                    //entire string could be parsed as a datetime
                    variant_setDateTimeExtended(variant, &dt);
                    goto defaultok;
                }
            }
            if (!variant_setChar(variant, str0))
                goto leave;
defaultok:
            *str = strtmp;
            break;
    }
    while (*str && *str <= ' ') // ignore white space and special characters
        str++;
    ret = parse_success;
    if (endptr)
        *endptr = str;
leave:
    return ret;
}

parse_code_t variant_parse(variant_t *variant, char *data, char **endptr) {
    return variant_setJson(variant, data, endptr, NULL);
}

bool variant_cliprint(string_t *data, const variant_t *variant, reqflags_t *flags) {
    string_clear(data);
    uint32_t attributes = 0;
    static const char *shell_default_delimiters[] = { "", "=", "", "", "", "", "", NULL};
    static const char *shell_uglified_delimiters[] = { "", "=", " ", "", "", " ", "", NULL};
    const char **delimiters = NULL;
    if (flags->json) {
        attributes |= variant_print_json;
    } else if (flags->shellify) {
        delimiters = flags->uglify ? shell_uglified_delimiters : shell_default_delimiters;
        attributes |= variant_print_bool_to_num;
        if (!flags->uglify)
            attributes |= variant_print_need_newline | variant_print_multiline;
    } else {
        attributes |= variant_print_bool_to_num;
    }
    if (!flags->uglify && !flags->shellify)
        attributes |= variant_print_beautify | variant_print_need_newline;
    return variant_print(data, variant, attributes, delimiters);
}

void object_details(object_t *object, string_t *s) {
    int n = 0;
    string_clear(s);
    if (object_attributes(object) & object_attr_read_only)
        string_appendFormat(s, "%s%s", n++?",":" <", "read-only");
    if (object_attributes(object) & object_attr_template)
        string_appendFormat(s, "%s%s", n++?",":" <", "template");
    if (object_attributes(object) & object_attr_instance)
        string_appendFormat(s, "%s%s", n++?",":" <", "instance");
    if (object_attributes(object) & object_attr_accept_parameters)
        string_appendFormat(s, "%s%s", n++?",":" <", "accept-parameters");
    if (object_attributes(object) & object_attr_persistent) {
        if (object_attributes(object) & object_attr_upc) {
            if (object_attributes(object) & object_attr_upc_usersetting) {
                string_appendFormat(s, "%s%s", n++?",":" <", "usersetting");
            }
            else {
                string_appendFormat(s, "%s%s", n++?",":" <", "upc");
            }
            if (object_attributes(object) & object_attr_upc_changed) {
                string_appendFormat(s, "%s%s", n++?",":" <", "upc_changed");
            }
            if (object_attributes(object) & object_attr_upc_overwrite) {
                string_appendFormat(s, "%s%s", n++?",":" <", "upc_overwrite");
            }
        }
        else {
            string_appendFormat(s, "%s%s", n++?",":" <", "persistent");
        }
    }
    if (n)
        string_appendChar(s, ">");
}

void parameter_details(parameter_t *parameter, string_t *s) {
    int n = 0;
    string_clear(s);
    string_appendFormat(s, ":%s", parameter_typeName(parameter));
    if (parameter_attributes(parameter) & parameter_attr_read_only)
        string_appendFormat(s, "%s%s", n++?",":" <", "read-only");
    if (parameter_attributes(parameter) & parameter_attr_template_only)
        string_appendFormat(s, "%s%s", n++?",":" <", "template-only");
    if (parameter_attributes(parameter) & parameter_attr_persistent) {
        if (parameter_attributes(parameter) & parameter_attr_upc) {
            if (parameter_attributes(parameter) & parameter_attr_upc_usersetting) {
                string_appendFormat(s, "%s%s", n++?",":" <", "usersetting");
            }
            else {
                string_appendFormat(s, "%s%s", n++?",":" <", "upc");
            }
            if (parameter_attributes(parameter) & parameter_attr_upc_changed) {
                string_appendFormat(s, "%s%s", n++?",":" <", "upc_changed");
            }
            if (parameter_attributes(parameter) & parameter_attr_upc_overwrite) {
                string_appendFormat(s, "%s%s", n++?",":" <", "upc_overwrite");
            }
        }
        else {
            string_appendFormat(s, "%s%s", n++?",":" <", "persistent");
        }
    }
    if (n)
        string_appendChar(s, ">");
    parameter_validator_t *validator = parameter_getValidator(parameter);
    if (validator) {
        switch (param_validator_type(validator)) {
            case parameter_validator_enum: {
                n = 0;
                string_appendChar(s, " enum [");
                variant_list_iterator_t *it;
                variant_list_for_each(it, param_validator_values(validator)) {
                    char *charvalue = variant_char(variant_list_iterator_data(it));
                    string_appendFormat(s, "%s%s", n++?", ":"", charvalue);
                    free(charvalue);
                }
                string_appendChar(s, "]");
                break;
            }
            case parameter_validator_range: {
                char *minvalue = variant_char(param_validator_minimum(validator));
                char *maxvalue = variant_char(param_validator_maximum(validator));
                string_appendFormat(s, " range [%s, %s]", minvalue, maxvalue);
                free(minvalue);
                free(maxvalue);
                break;
            }
            case parameter_validator_minimum: {
                char *minvalue = variant_char(param_validator_minimum(validator));
                string_appendFormat(s, " minvalue %s", minvalue);
                free(minvalue);
                break;
            }
            case parameter_validator_maximum: {
                char *maxvalue = variant_char(param_validator_maximum(validator));
                string_appendFormat(s, " maxvalue %s", maxvalue);
                free(maxvalue);
                break;
            }
            default:
                break;
        }
    }
}

void function_details(function_t *function, string_t *s) {
    int n2 = 0;
    string_clear(s);
    string_appendChar(s, "(");
    function_argument_t *funarg;
    int n1 = 0;
    for (funarg = function_firstArgument(function); funarg; funarg = function_nextArgument(funarg)) {
        string_appendFormat(s, "%s%s:%s", n1++?", ":"", argument_name(funarg), argument_typeName(funarg));
        n2 = 0;
        if (argument_attributes(funarg) & argument_attr_in)
            string_appendFormat(s, "%s%s", n2++?",":" <", "in");
        if (argument_attributes(funarg) & argument_attr_out)
            string_appendFormat(s, "%s%s", n2++?",":" <", "out");
        if (argument_attributes(funarg) & argument_attr_mandatory)
            string_appendFormat(s, "%s%s", n2++?",":" <", "mandatory");
        if (n2)
            string_appendChar(s, ">");
    }
    string_appendFormat(s, "):%s", function_typeName(function));
    if (function_attributes(function) & function_attr_template_only)
        string_appendFormat(s, "%s%s", n2++?",":" <", "template-only");
    if (n2)
        string_appendChar(s, ">");
}

void notification_details(notification_t *notification, string_t *s, reqflags_t *flags) {
    notification_parameter_t *param;
    string_t svar;
    string_initialize(&svar, 64);
    int n = 0;
    string_clear(s);
    for (param = notification_firstParameter(notification); param; param = notification_nextParameter(param)) {
        variant_cliprint(&svar, notification_parameter_variant(param), flags);
        string_appendFormat(s, "%s%s=%s", n++?", ":"{", notification_parameter_name(param), string_buffer(&svar));
    }
    if (n)
        string_appendChar(s, "}");
    string_cleanup(&svar);
}




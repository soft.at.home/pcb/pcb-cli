# pcb-cli

## Summary

This application provides a CLI (Command Line Interface)
towards the PCB ecosystem it is connected to.

## Description

The application `pcb_cli` can connect to a PCB system bus.
Using the CLI, it is possible to inspect each plug-in's data
model, install update notifications and execute the RPC's
exported by the plug-in's.

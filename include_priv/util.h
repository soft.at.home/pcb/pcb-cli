/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __util_h__
#define __util_h__

#include <stdarg.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#define PCB_CLI_TRACE_LEVEL TRACE_LEVEL_ERROR

typedef struct _reqflags {
    unsigned indexpath:1;
    unsigned slashpath:1;
    unsigned persistent:1;
    unsigned non_persistent:1;
    unsigned define:1;
    unsigned zip:1;
    unsigned less:1;
    unsigned quiet:1;
    unsigned json:1;
    unsigned validateonly:1;
    unsigned no_update_notifications:1;
    unsigned no_add_notifications:1;
    unsigned no_delete_notifications:1;
    unsigned no_custom_notifications:1;
    unsigned notifications_only:1;
    unsigned no_object_updates:1;
    unsigned untrusted:1;
    unsigned shellify:1;
    unsigned uglify:1;
    unsigned wait:1;
    unsigned template_info:1;
    unsigned alias:1;
    unsigned precise_time_stamps:1;
} reqflags_t;
#define reqflags_initializer {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}

typedef enum {
    parse_success,
    parse_error,
    parse_we_want_more
} parse_code_t;

int vslprintf(char * buf, int buflen, const char * fmt, va_list args);
int slprintf(char * buf, int buflen, const char * fmt, ...);
int slinsert(char * buf, int buflen, const char * str);

bool parseFlag(bool *target, const char *str);

bool parseCommandFlags(reqflags_t *flags, const char *str, const char **endptr);

parse_code_t variant_parse(variant_t *variant, char *data, char **endptr);
bool variant_cliprint(string_t *data, const variant_t *variant, reqflags_t *flags);

void object_details(object_t *object, string_t *s);
void parameter_details(parameter_t *parameter, string_t *s);
void function_details(function_t *function, string_t *s);
void notification_details(notification_t *notification, string_t *s, reqflags_t *flags);

#endif


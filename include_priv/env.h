/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
** 
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
** 
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
** 
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
** 
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
** 
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
** 
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
** 
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
** 
** DISCLAIMER
** 
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#ifndef __env_h__
#define __env_h__

#include <stdbool.h>

#include <components.h>

#include <pcb/utils.h>
#include <pcb/core.h>

#include "util.h"

typedef struct _env {
    char uri[128];
    char name[64];
    char user[64];
    reqflags_t reqflags;
    int depth;
    char path[256];
    bool interactive;
    bool exit;
    bool exitonerr;
    bool fireandstop;
    bool fireandforget;
    bool verbose;
    bool globalnotifications;
#if !defined(CONFIG_PCB_CLI_STATIC) && defined(CONFIG_PCB_HTTP_SERIALIZER)
    bool rpc_ws_output;
    int ws_version;
#endif
    int workload;
    pcb_t *pcb;
    peer_info_t *peer;
    bool daemon;
    char pidfile[256];
    int request_timeout;
    connection_info_t *input;
} env_t;

extern env_t globalenv;

extern char cmdbuf[8192];

extern void (*beforeOutput)();
extern void (*afterOutput)();

void beginOutput();
void endOutput();

void output(const char * fmt, ...); // cli output: send to stdout
void usrinf(const char * fmt, ...); // info msg to user: send to stderr
void logerr(const char * fmt, ...); // err msg: syslog and stderr if interactive
void loginf(const char * fmt, ...); // inf msg: only to stderr if interactive

bool setEnv(const char * param, const char * value, bool ignorecase);
bool isEnv(const char * param, bool ignorecase);

#endif

